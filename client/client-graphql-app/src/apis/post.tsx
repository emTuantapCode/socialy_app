import { gql } from "@apollo/client"

export const GET_Post = gql`
    query getPost($searching: filerPost!) {
      getPost(searching: $searching) {
        _id
        color
        author{
          _id
          name 
          avatar{
            filename
            src
          }
        }
        content 
        felling 
        media{
          filename
          mediasrc
        }
        favorited
        favoriters{
          _id
          name
          avatar{
            filename
            src
          }
        }
        favoriteNumber
        commentNumber
        comment{
          _id
          content
          favorite
          favoriteNumber
          createdAt
          author{
            _id
            name 
            avatar{
              filename
              src
            }
          }
        }
      }
    }
  `

export const FAVORITE_Post = gql`
  mutation changeFavoritePost($_id: ID!) {
    changeFavoritePost(_id: $_id) {
      favorited
      favoriters{
        _id
        name
        avatar{
          filename
          src
        }
      }
      favoriteNumber
    }
  }
`

export const CREATE_Post = gql`
    mutation createPost($color: String! , $content: String! , $felling: String, $media: [MediaObject]) {
      createPost(color: $color, content: $content, felling: $felling, media: $media) {
        media{
            filename
            mediasrc
        }
      }
    }
  `
