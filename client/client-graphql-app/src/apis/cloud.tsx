import { RcFile } from 'antd/lib/upload/interface'
import axios from "axios"
import { sizedAvatar } from '../constants/helpers/image'
interface Data { myfile: string | RcFile | Blob }
export interface MultilData { myfiles: string[] | RcFile[] | Blob[] }

export const Cloud = {
    single: async (data: Data) => {
        const response = await axios({
            method: 'post',
            url: 'http://localhost:5568/api/cloudinary/single-upload',
            headers: {
                "Content-Type": 'multipart/form-data; boundary=<calculated when request is sent>',
                "authorization": window.localStorage.getItem('token')
            },
            data
        })
        return {
            filename: response?.data?.filename,
            src: sizedAvatar(response?.data?.src)
        }
    },
    multil: async (data: MultilData) => {
        const response = await axios({
            method: 'post',
            url: 'http://localhost:5568/api/cloudinary/multil-upload',
            headers: {
                "Content-Type": 'multipart/form-data; boundary=<calculated when request is sent>',
                "authorization": window.localStorage.getItem('token')
            },
            data
        })
        return response
    }
}