import { gql } from "@apollo/client"

export const GET_Notifications = gql`
    query getNotifications {
        getNotifications {
            _id
            type
            source {
            _id
            name
            avatar{
                filename
                src
            }
            }
            destination {
            _id
            name
            avatar{
                filename
                src
            }
            }
            status
            uri
        }
    }
`