import axiosInstance from "../constants/axios";

export const Auth = {
    passport: async () => {
        const url = '/google/callback'
        const response = await axiosInstance.get(url)
        return response
    }
}
