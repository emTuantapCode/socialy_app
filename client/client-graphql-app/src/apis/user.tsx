import { gql } from '@apollo/client';

export const GET_Profile = gql`
    query getProfile($searching: ProfileFilter!) {
      getProfile(searching: $searching) {
        _id
        name
        bio
        phone
        address
        typeLogin
        age
        email
        status
        gender
        avatar {
          filename
          src
        }
        role_code
      }
    }
  `
export const GET_Profile_ByID = gql`
query getProfile($searching: ProfileFilter!) {
  getProfile(searching: $searching) {
    _id
    name
    bio
    phone
    address
    typeLogin
    age
    email
    status
    gender
    avatar {
      filename
      src
    }
    posts{
      _id
        color
        author{
          _id
          name 
          avatar{
            filename
            src
          }
        }
        content 
        felling 
        media{
          filename
          mediasrc
        }
        favorited
        favoriters{
          _id
          name
          avatar{
            filename
            src
          }
        }
        favoriteNumber
        commentNumber
        comment{
          _id
          content
          favorite
          favoriteNumber
          createdAt
          author{
            _id
            name 
            avatar{
              filename
              src
            }
          }
        }
    }
  }
}
`

export const UPDATE_Profile = gql`
    mutation updateProfile($data: Profile!) {
      updateProfile(data: $data) {
        name
        bio
        phone
        age
        email
        status
        gender
        avatar {
          filename
          src
        }
        role_code
      }
    }
  `
export const GET_Users = gql`
  query getUsers($searching: UsersFilter!) {
    getUsers(searching: $searching) {
      name
      bio
      phone
      age
      email
      status
      gender
      avatar {
        filename
        src
      }
    }
  }
`