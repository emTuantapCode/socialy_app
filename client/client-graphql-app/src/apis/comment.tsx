import { gql } from "@apollo/client"

export const CREATE_Comment = gql`
    mutation createComment($data: fieldsComment) {
      createComment(data: $data) {
        post{
            commentNumber
            comment{
                _id
                content
                favorite
                favoriteNumber
                createdAt
                author{
                    _id
                    name 
                    avatar{
                    filename
                    src
                    }
                }
            }
        }
      }
    }
`
export const DELETE_Comment = gql`
    mutation deleteComment($_id: ID!) {
      deleteComment(_id: $_id) {
        post{
            commentNumber
            comment{
                _id
                content
                favorite
                favoriteNumber
                createdAt
                author{
                    _id
                    name 
                    avatar{
                    filename
                    src
                    }
                }
            }
        }
      }
    }
`
export const FAVORITE_Comment = gql`
    mutation changeFavoriteComment($_id: ID!) {
      changeFavoriteComment(_id: $_id) {
        post{
            commentNumber
            comment{
                _id
                content
                favorite
                favoriteNumber
                createdAt
                author{
                    _id
                    name 
                    avatar{
                    filename
                    src
                    }
                }
            }
        }
      }
    }
`