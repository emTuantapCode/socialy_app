// @ts-ignore
import ReactDOM from 'react-dom/client';
// @ts-ignore
import App from './App.tsx'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { createUploadLink } from 'apollo-upload-client'
import { Provider as ReduxProvider } from 'react-redux';
import store from './store/store'
import './index.css'

const link = createUploadLink({
  uri: `${import.meta.env.VITE_SERVER_URI}/graphql`
});

const authLink = setContext((_, { headers }) => {
  const token = window.localStorage.getItem('token') || null
  return {
    headers: {
      ...headers,
      authorization: token
    },
  };
});

const client = new ApolloClient({
  link: authLink.concat(link),
  cache: new InMemoryCache({
    addTypename: false
  })
});

ReactDOM.createRoot(document.getElementById('root')!).render(
  <ReduxProvider store={store}>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </ReduxProvider>,
)
