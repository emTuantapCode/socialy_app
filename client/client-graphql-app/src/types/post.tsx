import type { User } from "./user"
import type { Comment } from "./comment"
export interface PostProps {
    open: boolean,
    handleOk?: VoidFunction,
    confirmLoading?: boolean,
    handleCancel: VoidFunction
}

export interface LayoutProps {
    urls: string[]
}

export interface Media {
    filename?: string,
    mediasrc?: string
}
export interface Post {
    _id?: string,
    color?: string,
    author?: User,
    content?: string,
    felling?: string,
    media?: Media[] | [],
    favorited?: boolean,
    commentNumber?: number,
    favoriters?: User[] | [],
    favoriteNumber?: number,
    comment?: Comment[] | []
}

export interface PostComponent {
    post: Post,
}