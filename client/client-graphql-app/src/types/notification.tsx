import type { User } from "./user"
import type { ReactElement } from "react"

export interface Notify {
    _id: string,
    type: string,
    source: User,
    destination: User,
    status: string,
    uri: string
}

export interface NotifyFormated {
    _id: string,
    type: string,
    source: User,
    destination: User,
    status: string | boolean,
    uri: string,
    content: string | undefined,
    typeIcon: ReactElement | undefined
}