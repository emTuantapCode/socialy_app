import type { User } from "./user";

export interface Comment {
    _id?: string,
    content?: string,
    favorite?: [User] | [],
    favoriteNumber?: number,
    createdAt?: string,
    author?: User
}