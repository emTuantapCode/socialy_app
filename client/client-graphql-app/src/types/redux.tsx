type Data = string
export interface Action {
    type: string,
    data: Data
}