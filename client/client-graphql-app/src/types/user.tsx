import type { Post } from "./post";
export interface Avatar {
    filename?: string | undefined,
    src: string | undefined
}

export interface ProfileState {
    name?: string,
    age?: number,
    email?: string,
    bio?: string,
    phone?: string,
    address?: string,
    avatar?: Avatar,
    gender?: string,
    status?: string,
    role_code?: string
}

export interface User {
    _id?: string,
    name?: string,
    age?: number,
    email?: string,
    bio?: string,
    phone?: string,
    address?: string,
    avatar?: Avatar,
    gender?: string,
    status?: string,
    posts?: Post[]
}

export interface UserProps {
    data: User
}