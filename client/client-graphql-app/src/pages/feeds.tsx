import { useQuery } from '@apollo/client';
import { Avatar, Button, Col, Row } from "antd";
import * as React from 'react';
import {
    AiOutlineFileImage, AiOutlineVideoCameraAdd
} from 'react-icons/ai';
import { BiHappyBeaming } from 'react-icons/bi';
import type { FixTypeLater } from "react-redux";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { GET_Post } from '../apis/post';
import EmtyPost from '../components/items/emty_post';
import EventCard from '../components/items/event_card';
import LazyPost from '../components/items/lazy_post';
import Post from '../components/items/post';
import PostCreater from '../components/items/post_creater';
import FriendList from '../components/layouts/friend_list';

const NewFeeds = () => {
    const { user } = useSelector((state: FixTypeLater) => state.user)
    const [open, setOpen] = React.useState(false);
    const [posts, setPosts] = React.useState([])
    const handleCancel = () => setOpen(false);
    const { loading } = useQuery(GET_Post, {
        variables: { searching: {} },
        onCompleted: (data) => {
            setPosts(preve => preve.concat(data.getPost))
        }
    });

    return (
        <Row>
            <Container span={18}>
                <Row>
                    <Col span={16}>
                        <CenterCol>
                            <PostPopup>
                                <Avatar size={50} src={user?.avatar?.src} />
                                <InputPost onClick={() => setOpen(true)}>What's happening?</InputPost>
                                <FlexBox>
                                    <IconBox onClick={() => setOpen(true)}><AiOutlineVideoCameraAdd size={18} /> <span>Vidoes</span></IconBox>
                                    <IconBox onClick={() => setOpen(true)}><AiOutlineFileImage size={18} /> <span>Photo</span></IconBox>
                                    <IconBox onClick={() => setOpen(true)}><BiHappyBeaming size={18} /> <span>Feeling</span></IconBox>
                                    <Button type="primary" size={'middle'} onClick={() => setOpen(true)}>
                                        Post
                                    </Button>
                                </FlexBox>
                            </PostPopup>
                            <PostCreater
                                open={open}
                                handleCancel={handleCancel}
                            />
                            {posts.length === 0 && <EmtyPost />}
                            {posts.length !== 0 && posts.map(post => <Post post={post} />)}
                            {loading && <LazyPost />}
                        </CenterCol>
                    </Col>
                    <Col style={{ position: 'fixed', right: '20.6%', width: '22.4%' }} span={6}>
                        <EventCard />
                    </Col>
                </Row>
            </Container>
            <FriendList />
        </Row>
    )
}
const Container = styled(Col)`
    background-color: #f9fafb;
    border-radius: 24px;
    padding-top: 24px;
    min-height: 800px;
    &::-webkit-scrollbar {
        width: 0;
    }
`
const CenterCol = styled(Col)`
    width: 90%;
    margin: 0 auto;
`
const IconBox = styled.div`
    display: flex;
    align-items:center;
    &:hover{
        cursor: pointer;
    }
`
const PostPopup = styled.div`
    width: 100%;
    background-color: #fff;
    border-radius: 12px;
    padding: 12px 12px;
    color: #4e5d78;
    margin-bottom: 12px;
`
const InputPost = styled.div`
    display: inline-block;
    background-color: #f9fafb;
    width: 85.5%;
    padding: 12px;
    border-radius: 8px;
    margin-left: 12px;
    letter-spacing: 1px;
    &:hover {
        cursor: pointer;
        background-color: #f9fafb;
        color: #ccc;
    }
`
const FlexBox = styled.div`
    margin-top: 16px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`

export default NewFeeds
