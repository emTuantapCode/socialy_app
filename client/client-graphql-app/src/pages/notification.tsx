import * as React from "react"
import { Row, Col, Avatar, Dropdown } from "antd"
import type { MenuProps } from 'antd';
import styled from "styled-components"
import type { MenuInfo } from "rc-menu/lib/interface";
import FriendList from "../components/layouts/friend_list"
import { AiTwotoneSetting } from "react-icons/ai"
import { BsDot } from 'react-icons/bs'
import { useQuery } from "@apollo/client"
import { GET_Notifications } from "../apis/notification"
import type { NotifyFormated } from "../types/notification"
import { notifyFormater } from "../constants/helpers/format_totify"
import { TbGlassFullFilled } from 'react-icons/tb'
import avatar from '../assets/component/avatar.jpeg'

const items: MenuProps['items'] = [
    {
        label: 'All (default)',
        key: 'All',
    },
    {
        label: 'Read',
        key: 'Read',
    },
    {
        label: 'Unread',
        key: 'Unread',
    },
    {
        label: 'Comment',
        key: 'Comment',
    },
    {
        label: 'Message',
        key: 'Message',
    },
    {
        label: 'Add Friend',
        key: 'Add Friend',
    },
    {
        label: 'Like',
        key: 'Like',
    },
    {
        label: 'Like Comment',
        key: 'Like Comment',
    },
    {
        label: 'New Post',
        key: 'New Post',
    },
];

const Notification = () => {
    const [notifications, setNotifications] = React.useState<NotifyFormated[] | []>([])
    const [originData, setOriginData] = React.useState<NotifyFormated[] | []>([])
    const { loading } = useQuery(GET_Notifications, {
        onCompleted: (data) => {
            const formatedData = notifyFormater(data.getNotifications)
            setNotifications(formatedData)
            setOriginData(formatedData)
        }
    });

    const handleMenuClick = (e: MenuInfo) => {
        switch (e.key) {
            case 'All': {
                return setNotifications(originData)
            }
            case 'Read': {
                const filterData: NotifyFormated[] | [] = originData.filter((data) => data.status === false)
                return setNotifications(filterData)
            }
            case 'Unread': {
                const filterData: NotifyFormated[] | [] = originData.filter((data) => data.status === true)
                return setNotifications(filterData)
            }
            default: {
                const filterData: NotifyFormated[] | [] = originData.filter((data) => data.type === e.key)
                return setNotifications(filterData)
            }
        }
    }

    return (
        <Row>
            <Container span={18}>
                <NotifyList>
                    <StyledCard>
                        <StyledP>Notifications</StyledP>
                        <StyledP>
                            <Dropdown menu={{ items, onClick: handleMenuClick }} trigger={['click']}>
                                <AiTwotoneSetting />
                            </Dropdown>
                        </StyledP>
                    </StyledCard>
                    {notifications.length === 0 && <StyledCard>
                        <Wrapper>
                            <StyledIcon><TbGlassFullFilled color="#ffab00" size={22} /></StyledIcon>
                            <StyledAvatar src={avatar} size={36} />
                            <InlineBlock>
                                <ContentNotify>Great ! you caught up all notifications about it</ContentNotify>
                                <div>now</div>
                            </InlineBlock>
                        </Wrapper>
                        <StyledDot color='#38cb89' />
                    </StyledCard>}
                    {!loading && notifications && notifications.map(notify => {
                        //@ts-ignore
                        const id = notify._id
                        return (
                            <StyledCard key={id}>
                                <Wrapper>
                                    <StyledIcon>{notify.typeIcon}</StyledIcon>
                                    <StyledAvatar src={notify?.source?.avatar?.src} size={36} />
                                    <InlineBlock>
                                        <ContentNotify>{notify.content}</ContentNotify>
                                        <div>1m ago</div>
                                    </InlineBlock>
                                </Wrapper>
                                <StyledDot color={notify?.status ? '#ff5630' : ''} />
                            </StyledCard>
                        )
                    })}
                </NotifyList>
            </Container>
            <FriendList />
        </Row>
    )
}

const Container = styled(Col)`
    background-color: #f9fafb;
    border-radius: 24px;
    padding-top: 24px;
    min-height: 600px;
    &::-webkit-scrollbar {
        width: 0;
    }
`
const NotifyList = styled(Row)`
    width: 80%;
    margin: 0 auto;
    background-color: #fff;
    border-radius: 12px;
`
const StyledCard = styled.div`
    width: 100%;
    padding: 0 12px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 3px solid #f9fafb;
`
const StyledP = styled.p`
    font-size: 18px;
    font-weight: 600;
    padding: 8px 0;
`
const Wrapper = styled.div`
    padding: 8px 0;
    display: flex;
    align-items: center;
`
const StyledAvatar = styled(Avatar)`
    margin: 0 12px;
`
const StyledDot = styled(BsDot)`
    font-size: 38px;
    font-weight: 800;
`
const StyledIcon = styled.div`
    display: inline-block;
    font-size: 18px;
    min-width: 22px;
`
const InlineBlock = styled.div`
    display: inline-block;
    align-content: center;
`
const ContentNotify = styled.div`
    font-size: 16px;
    font-weight: 600;
    &:hover{
        cursor: pointer;
    }
`
export default Notification
