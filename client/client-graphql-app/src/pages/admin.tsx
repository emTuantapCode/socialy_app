import { useSelector } from "react-redux"
import type { FixTypeLater } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";

const Admin = () => {
    const navigate = useNavigate()
    const { user } = useSelector((state: FixTypeLater) => state.user)
    useEffect(() => {
        const isClient = !(user?.role_code === import.meta.env.VITE_SECRET_ROLE)
        if (isClient) {
            navigate('/not-found')
        }
    }, [navigate, user])

    return (
        <>this is Admin page</>
    )
}

export default Admin
