import { Alert, Spin } from 'antd';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux'
import { GET_Profile, UPDATE_Profile } from '../apis/user';
import { useQuery, useMutation } from '@apollo/client';
import { Col, Row, Space, Input, Radio, Button, RadioChangeEvent, Upload } from 'antd'
import { InboxOutlined } from '@ant-design/icons'
import { ProfileState, Avatar } from '../types/user'
import { UploadRequestOption as RcCustomRequestOptions } from 'rc-upload/lib/interface'
import { Cloud } from '../apis/cloud';
import * as action from '../store/actions'
import styled, { css } from 'styled-components';

const trueStatus = 'true'
const blankInfo = 'fill your info here...'
const { Dragger } = Upload

const LoginSuccess = () => {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const url = new URLSearchParams(window.location.search)
    const [inputValue, setInputValue] = useState<ProfileState>({ name: '' })
    const [disabledButon, setDisabledButon] = useState(true)
    const [uploader, setUploader] = useState(false)
    const [avatarCustom, setAvatarCustom] = useState<Avatar | undefined>(undefined)
    const isNew = url.get('is-new') === trueStatus
    const token = url.get('token')
    token && window.localStorage.setItem('token', token)
    const { loading, error, data } = useQuery(GET_Profile, {
        variables: { searching: {} }
    });
    const [updateProfile, { error: responseError }] = useMutation(UPDATE_Profile, {
        onCompleted: (data) => {
            dispatch(action.getUserInfo(data?.updateProfile))
            navigate('/new-feeds')
        }
    });

    if (!inputValue?.name && data?.getProfile) {
        const commonData = { ...data?.getProfile }
        delete commonData.avatar
        setInputValue(commonData);
    }

    useEffect(() => {
        if (isNew) return
        setTimeout(() => {
            navigate('/new-feeds')
        }, 1800)
    }, [isNew, navigate])

    dispatch(action.getUserInfo(data?.getProfile))
    dispatch(action.getCurrentProfile(data?.getProfile?._id))

    if (error || responseError) return <Container>Error: {error ? error?.message : responseError?.message}</Container>
    const handleChangeInput = (e: React.ChangeEvent<HTMLInputElement> | RadioChangeEvent, type: string) => {
        const newValue = { ...inputValue, [type]: e.target.value }
        setInputValue(newValue)
        if (newValue.address && newValue.age && newValue.bio && newValue.gender && newValue.phone && newValue.status)
            setDisabledButon(false)
    }

    const handleSubmit = () => {
        if (avatarCustom) {
            const buffer: ProfileState = inputValue
            buffer['avatar'] = avatarCustom
            setInputValue(buffer)
        }
        delete inputValue['role_code']
        updateProfile({
            variables: { data: inputValue }
        })
    }

    const handleUploader = async (info: RcCustomRequestOptions) => {
        const data = { myfile: info.file }
        const response = await Cloud.single(data)
        setAvatarCustom(response)
    }

    return (
        <>
            {(!isNew || loading) && <Container>
                <Spin tip="Loading...">
                    <Alert
                        message="Waiting for client process"
                        description="please give me sometime for repairing"
                        type="info"
                    />
                </Spin>
            </Container>}
            {isNew && <Container>
                <BoxAvatar>
                    <StyledImg src={avatarCustom?.src || data?.getProfile?.avatar?.src} alt={avatarCustom?.filename || data?.getProfile?.avatar?.filename} />
                    <UploadButton onClick={() => setUploader(!uploader)}>
                        <svg width="25" height="20" viewBox="0 0 15 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M10.9573 2.98981H11.0073C11.4007 2.98987 11.7902 3.06755 12.1534 3.21838C12.5167 3.36921 12.8466 3.59024 13.1243 3.86881C13.686 4.43178 14.0015 5.19456 14.0015 5.98981C14.0015 6.78505 13.686 7.54783 13.1243 8.11081C12.8466 8.38937 12.5167 8.6104 12.1534 8.76123C11.7902 8.91207 11.4007 8.98974 11.0073 8.98981H9.01234V7.98981H11.0073C11.5223 7.96677 12.0085 7.74599 12.3648 7.37344C12.7211 7.00089 12.9199 6.50528 12.9199 5.98981C12.9199 5.47433 12.7211 4.97872 12.3648 4.60617C12.0085 4.23362 11.5223 4.01284 11.0073 3.98981H10.0933L9.97034 3.13281C9.8953 2.59588 9.64707 2.09812 9.26335 1.71513C8.87964 1.33213 8.38141 1.08484 7.84435 1.01081C7.30696 0.937397 6.76034 1.04232 6.28833 1.30949C5.81633 1.57665 5.44501 1.99129 5.23134 2.48981L4.89834 3.25181L4.08934 3.06281C3.90442 3.01694 3.71485 2.99244 3.52434 2.98981C2.86234 2.98981 2.22734 3.25281 1.76034 3.72181C1.41231 4.07175 1.17551 4.51678 1.0797 5.00094C0.98389 5.4851 1.03335 5.98677 1.22187 6.4429C1.41039 6.89902 1.72955 7.28923 2.13923 7.56447C2.5489 7.83971 3.0308 7.98769 3.52434 7.98981H6.01834V8.98981H3.52434C3.02903 8.99426 2.53846 8.89311 2.0853 8.69309C1.63215 8.49306 1.22683 8.19877 0.896345 7.82981C0.396652 7.27333 0.0882371 6.57149 0.0162592 5.82706C-0.0557187 5.08263 0.112521 4.33471 0.496345 3.69281C0.750561 3.26753 1.09225 2.90113 1.49876 2.61788C1.90528 2.33463 2.36734 2.14099 2.85434 2.04981C3.34034 1.95981 3.84134 1.97281 4.32234 2.09081C4.62543 1.39682 5.14626 0.820404 5.80606 0.448735C6.46587 0.0770666 7.22875 -0.0696396 7.97934 0.0308051C8.73025 0.132416 9.42744 0.476409 9.96496 1.0105C10.5025 1.54459 10.8509 2.23957 10.9573 2.98981ZM9.29434 6.8428L7.98034 5.52981V10.9658H6.98634V5.56581L5.70834 6.84381L5.00134 6.13581L7.14734 3.98981H7.85534L10.0013 6.13581L9.29434 6.8428Z"
                                fill="#fff" />
                        </svg>
                    </UploadButton>
                    <StyledDragegr $display={uploader} name='file' multiple={false} customRequest={(info: RcCustomRequestOptions) => handleUploader(info)} showUploadList={false}>
                        <p className="ant-upload-drag-icon">
                            <InboxOutlined />
                        </p>
                        <p className="ant-upload-text">Click or drag file to this area to upload and wait some seconds</p>
                        <p className="ant-upload-hint">
                            Support for a single or bulk upload. Strictly prohibited from uploading company data or other
                            banned files.
                        </p>
                    </StyledDragegr>
                </BoxAvatar>
                <StyledRow>
                    <Col span={12}>
                        <StyledSpace>
                            <Input style={{ width: '20%' }} defaultValue="Name" disabled />
                            <Input style={{ width: '80%' }} value={inputValue?.name || ''} onChange={(e) => handleChangeInput(e, 'name')} placeholder={blankInfo} />
                        </StyledSpace>
                        <StyledSpace>
                            <Input style={{ width: '20%' }} defaultValue="Email" disabled />
                            <Input style={{ width: '80%' }} value={inputValue.email || ''} onChange={(e) => handleChangeInput(e, 'email')} placeholder={blankInfo} />
                        </StyledSpace>
                        <StyledSpace>
                            <Input style={{ width: '20%' }} defaultValue="Phone" disabled />
                            <Input style={{ width: '80%' }} value={inputValue.phone || ''} onChange={(e) => handleChangeInput(e, 'phone')} placeholder={blankInfo} />
                        </StyledSpace>
                        <StyledSpace>
                            <div>Gender</div>
                            <Radio.Group value={inputValue?.gender} onChange={(e) => handleChangeInput(e, 'gender')}>
                                <Radio.Button value="Male">Male</Radio.Button>
                                <Radio.Button value="Female">Female</Radio.Button>
                                <Radio.Button value="others">others</Radio.Button>
                                <Radio.Button value="Secret">Secret</Radio.Button>
                            </Radio.Group>
                        </StyledSpace>
                        <StyledSpace>
                            <div>Status</div>
                            <Radio.Group value={inputValue?.status} onChange={(e) => handleChangeInput(e, 'status')}>
                                <Radio.Button value="Single">Single</Radio.Button>
                                <Radio.Button value="Married">Married</Radio.Button>
                                <Radio.Button value="Loving">Loving</Radio.Button>
                                <Radio.Button value="Secret">Secret</Radio.Button>
                            </Radio.Group>
                        </StyledSpace>
                    </Col>
                    <Col span={12}>
                        <StyledSpace>
                            <Input style={{ width: '20%' }} defaultValue="Age" disabled />
                            <Input style={{ width: '80%' }} value={inputValue.age || ''} onChange={(e) => handleChangeInput(e, 'age')} placeholder={blankInfo} />
                        </StyledSpace>
                        <StyledSpace>
                            <Input style={{ width: '20%' }} defaultValue="Bio" disabled />
                            <Input style={{ width: '80%' }} value={inputValue.bio || ''} onChange={(e) => handleChangeInput(e, 'bio')} placeholder={blankInfo} />
                        </StyledSpace>
                        <StyledSpace>
                            <Input style={{ width: '20%' }} defaultValue="Address" disabled />
                            <Input style={{ width: '80%' }} value={inputValue.address || ''} onChange={(e) => handleChangeInput(e, 'address')} placeholder={blankInfo} />
                        </StyledSpace>
                        <StyledSpace>Complete your profile for join our community!</StyledSpace>
                        <StyledSpace>
                            <StyledButton onClick={() => handleSubmit()} type="primary" disabled={disabledButon}>Completed!</StyledButton>
                        </StyledSpace>
                    </Col>
                </StyledRow>
            </Container>}
        </>
    )
}

const Container = styled.div`
    padding: 5% 10%;
`

const StyledImg = styled.img`
    border-radius: 50%;
    width: 15%;
    height: 100%;
    border: solid 6px #a1a9b7;
    box-shadow: 0 0 50px #28282a;
    object-fit: cover;
`

const BoxAvatar = styled.div`
    position: relative;
`

const UploadButton = styled.div`
    position: absolute;
    top: 1%;
    left: 1%;
    color: #fff;
    background: #1677ff;
    padding: 8px 7px;
    border-radius: 50%;
    box-shadow: 0 0 10px #28282a;
    &:hover{
        cursor: pointer;
    }
    user-select: none;
`

const StyledRow = styled(Row)`
    margin-top: 44px;
`

const StyledSpace = styled(Space.Compact)`
    margin: 44px 0;
    display: block;
    width: 85%;
`

const StyledButton = styled(Button)`
    display: block;
    width: 40%;
    margin: 18% auto 0;
    padding:
`

const StyledDragegr = styled(Dragger)`
    ${(props) => {
        //@ts-ignore
        if (props.$display) return css`display: inline-block;`
        else return css`display: none;`
    }}
    margin-left: 12%;
`

export default LoginSuccess
