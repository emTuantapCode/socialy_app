import * as React from 'react'
import styled, { css } from "styled-components"
import BackgroundProfile from '../assets/backgorund/backgroundProfile.jpg'
import { useQuery } from '@apollo/client'
import { useNavigate } from 'react-router-dom'
import { GET_Profile_ByID } from '../apis/user'
import { Avatar, Button, Row, Col, Menu } from "antd"
import type { MenuProps } from "antd"
import type { FixTypeLater } from "react-redux";
import type { User } from '../types/user'
import { useSelector } from "react-redux";
import { FcEditImage } from 'react-icons/fc'
import { BsMessenger } from 'react-icons/bs'
import EventCard from "../components/items/event_card";
import LazyPost from '../components/items/lazy_post';
import {
    AiOutlineFileImage, AiOutlineVideoCameraAdd
} from 'react-icons/ai';
import { BiHappyBeaming } from 'react-icons/bi';
import PostCreater from '../components/items/post_creater';
import EmtyPost from '../components/items/emty_post';
import Post from '../components/items/post';
import {
    GlobalOutlined,
    UserOutlined,
    CarryOutOutlined,
    HomeOutlined,
    LinkOutlined,
    PhoneOutlined,
    CommentOutlined
} from '@ant-design/icons'
import { IntroInfo } from '../constants/app/system'

const Profile = () => {
    const navigate = useNavigate()
    const { user, _id } = useSelector((state: FixTypeLater) => state.user)
    const [open, setOpen] = React.useState(false);
    const [posts, setPosts] = React.useState([])
    const [profile, setProfile] = React.useState<User>(user)
    const handleCancel = () => setOpen(false);
    const { loading, refetch } = useQuery(GET_Profile_ByID, {
        variables: { searching: { _id: _id } },
        onCompleted: (data) => {
            setProfile(data?.getProfile)
            setPosts(data?.getProfile?.posts)
        }
    });
    //@ts-ignore
    const items: MenuProps['items'] = [
        GlobalOutlined,
        UserOutlined,
        CarryOutOutlined,
        HomeOutlined,
        LinkOutlined,
        PhoneOutlined,
        CommentOutlined
    ].map((icon, index) => ({
        icon: React.createElement(icon),
        //@ts-ignore
        label: profile[IntroInfo[index]],
    }));
    React.useEffect(() => {
        refetch({ searching: { _id: _id } })
    }, [_id, refetch])
    return (
        <>
            <ContainerHead>
                <ThumnailImg>
                    <StyledAvatar size={158} src={profile?.avatar?.src} />
                    <EditProfile $display={user?._id === profile?._id} onClick={() => navigate('/setting')}><FcEditImage /> Edit your profile</EditProfile>
                </ThumnailImg>
                <UserInfo>
                    <div>
                        <UserName>{profile?.name}</UserName>
                        <div>{profile?.bio}</div>
                    </div>
                    <TriggerGroup $display={user?._id !== profile?._id}>
                        <StyledButton type="primary" size={'large'}>+ Add Friend</StyledButton>
                        <StyledButton type="primary" size={'large'}><BsMessenger style={{ marginRight: '6px', marginBottom: '-4px' }} size={18} /> Message</StyledButton>
                    </TriggerGroup>
                </UserInfo>
            </ContainerHead>
            <ContainerBody>
                <StyledCol span={5}>
                    <IntroCard>
                        INTRO
                        <Menu mode="inline" items={items} selectable={false} />
                    </IntroCard>
                </StyledCol>
                <StyledCol span={13}>
                    <CenterCol>
                        <PostPopup>
                            <Avatar size={50} src={user?.avatar?.src} />
                            <InputPost onClick={() => setOpen(true)}>What's happening?</InputPost>
                            <FlexBox>
                                <IconBox onClick={() => setOpen(true)}><AiOutlineVideoCameraAdd size={18} /> <span>Vidoes</span></IconBox>
                                <IconBox onClick={() => setOpen(true)}><AiOutlineFileImage size={18} /> <span>Photo</span></IconBox>
                                <IconBox onClick={() => setOpen(true)}><BiHappyBeaming size={18} /> <span>Feeling</span></IconBox>
                                <Button type="primary" size={'middle'} onClick={() => setOpen(true)}>
                                    Post
                                </Button>
                            </FlexBox>
                        </PostPopup>
                        <PostCreater
                            open={open}
                            handleCancel={handleCancel}
                        />
                        {posts.length === 0 && <EmtyPost />}
                        {posts.length !== 0 && posts.map(post => <Post post={post} />)}
                        {loading && <LazyPost />}
                    </CenterCol>
                </StyledCol>
                <StyledCol span={6}><EventCard /></StyledCol>
            </ContainerBody>
        </>
    )
}

const ContainerHead = styled.div`
    border-radius: 24px;
    box-shadow: 0 4px 2px -2px #ccc;
    overflow: hidden;
    margin: 12px 12px 0 12px;
    color: #4e5d78;
`
const ThumnailImg = styled.div`
    width: 100%;
    height: 280px;
    background:url(${BackgroundProfile}); 
    background-position: center;
    background-size: cover;
    border-radius: 8px;
    position: relative;
`
const UserInfo = styled.div`
    height: 120px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 4%;
`
const StyledAvatar = styled(Avatar)`
    position: absolute;
    bottom: -32px;
    left: 4%;
    border: solid 4px #fff;
`
const EditProfile = styled(Button)`
    ${(props) => {
        //@ts-ignore
        if (props.$display) return css`display: block;`
        else return css`display: none;`
    }}
    position: absolute;
    bottom: 24px;
    right: 4%;
    border: none;
    color: #4e5d78;
    font-weight: 500;
`
const TriggerGroup = styled(Button)`
    ${(props) => {
        //@ts-ignore
        if (props.$display) return css`display: block;`
        else return css`display: none;`
    }}
`
const UserName = styled.div`
    font-size: 28px;
    font-weight: 600;
`
const StyledButton = styled(Button)`
    border: none;
    margin-left: 12px;
    font-size: 16px;
    font-weight: 500;
`
const ContainerBody = styled(Row)`
    background-color: #f9fafb;
    border-radius: 24px;
    margin: 24px 12px 0 12px;
    color: #4e5d78;
`
const IntroCard = styled.div`
    background-color: #fff;
    margin-left: 12px;
    padding-left: 12px;
    border-radius: 12px;
    font-size: 16px;
    font-weight: 700;
`
const StyledCol = styled(Col)`
    margin-top: 20px;
`
const CenterCol = styled(Col)`
    width: 96%;
    margin: 0 auto;
`
const IconBox = styled.div`
    display: flex;
    align-items:center;
    &:hover{
        cursor: pointer;
    }
`
const PostPopup = styled.div`
    width: 100%;
    background-color: #fff;
    border-radius: 12px;
    padding: 12px 12px;
    color: #4e5d78;
    margin-bottom: 12px;
`
const InputPost = styled.div`
    display: inline-block;
    background-color: #f9fafb;
    width: 85.5%;
    padding: 12px;
    border-radius: 8px;
    margin-left: 12px;
    letter-spacing: 1px;
    &:hover {
        cursor: pointer;
        background-color: #f9fafb;
        color: #ccc;
    }
`
const FlexBox = styled.div`
    margin-top: 16px;
    display: flex;
    justify-content: space-between;
    align-items: center;
`
export default Profile
