import * as React from "react"
import { Col, Row, Avatar } from "antd"
import styled from "styled-components"
import avatar from '../assets/component/avatar.jpeg'


const Blog = () => {
    const contents: string[] = [
        `  
    THE LEGACY OF DOS <br/>4/10/2023 · Reviews <br/> <br/>
    How the First Operating System Shaped the Future of Computing
    When we think about the history of personal computing, it’s impossible to ignore the role of DOS. The Disk Operating System, created by Microsoft in the early 1980s, was the first widely used operating system for IBM-compatible personal computers. In this post, we’ll take a look at how DOS shaped the future of computing and left a lasting legacy on the industry.
    <br/> <br/>
    STANDARDIZATION:<br/>
    Before DOS, there was a lack of standardization in the personal computing industry. Each computer manufacturer had its own proprietary operating system, which made it difficult for software developers to create programs that would work on all computers. DOS changed that by providing a standardized platform for software developers to work with.
    <br/> <br/>
    COMMAND LINE INTERFACE: <br/>
    DOS introduced the command line interface (CLI), which allowed users to interact with the computer by entering commands into a text-based interface. While modern graphical user interfaces (GUIs) have largely replaced the CLI, the skills and concepts learned from DOS still play a role in modern computing.
    <br/> <br/>
    SOFTWARE DEVELOPMENT:<br/>
    DOS made it easier for software developers to create and distribute programs. By providing a standardized platform and a common interface, DOS made it possible for developers to create software that could run on a wide variety of computers. This helped to drive the growth of the software industry and paved the way for the development of modern operating systems.
    <br/> <br/>
    COMPATIBILITY:<br/>
    DOS was designed to be compatible with a wide range of hardware configurations. This meant that users could install DOS on their existing computers, without the need to purchase new hardware. This helped to make personal computing more accessible to a wider audience.
    <br/> <br/>
    LEGACY: <br/>
    While DOS has been largely replaced by modern operating systems, its legacy lives on. Many of the concepts and technologies developed for DOS continue to be used in modern computing. In addition, there are still many legacy applications and systems that rely on DOS, particularly in industrial and scientific settings.
    <br/>
    In conclusion, DOS played a critical role in the early years of personal computing. It provided a standardized platform for software developers, introduced the CLI, and made computing more accessible to a wider audience. While DOS has been largely replaced by modern operating systems, its legacy continues to shape the future of computing.
    <br/> <br/>
    ¶¶¶¶¶
    
    computing / history / legacy / OS <br/>
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    <br/> <br/> <br/> <br/>
    
    THE IMPACT OF PC <br/>
    
    4/10/2023
    ·
    
    Opinions <br/> <br/>
    The Impact of Personal Computers on Society and Culture
    
    The invention of personal computers has transformed the way we live, work, and interact with each other. From the way we communicate to the way we consume media, personal computers have had a profound impact on society and culture. In this post, we’ll take a look at some of the ways in which personal computers have shaped our world.
    <br/> <br/>
    COMMUNICATION: <br/>
    Personal computers have revolutionized the way we communicate with each other. Email, instant messaging, and social media have made it easier than ever to stay connected with friends, family, and colleagues around the world.
    <br/> <br/>
    WORK:<br/>
    Personal computers have transformed the way we work. From word processing to spreadsheets, personal computers have made it easier for us to be productive and efficient in our jobs. The internet has also made it possible to work remotely, which has allowed people to work from anywhere in the world.
    <br/> <br/>
    ENTERTAINMENT:<br/>
    Personal computers have changed the way we consume media. We no longer have to rely on TV or radio schedules to watch or listen to our favorite shows or music. We can now stream content on-demand, whether it’s a movie, TV show, or music album.
    <br/><br/>
    EDUCATION:<br/>
    Personal computers have made education more accessible and interactive. With online learning platforms and digital textbooks, students can learn at their own pace and engage with course material in new and innovative ways.
    <br/><br/>
    CREATIVITY:<br/>
    Personal computers have enabled new forms of artistic expression. From graphic design to music production, personal computers have made it possible for people to create and share their work with others.
    <br/><br/>
    PRIVACY AND SECURITY:<br/>
    Personal computers have also brought new challenges when it comes to privacy and security. With the rise of online threats such as hacking and identity theft, it’s more important than ever to protect our personal information online.
    <br/>
    The impact of personal computers on society and culture cannot be overstated. They have transformed the way we communicate, work, entertain ourselves, learn, and express ourselves creatively. While there have been some challenges along the way, the benefits of personal computers are clear. They have made our lives easier, more efficient, and more connected. It’s exciting to think about what the future holds for personal computing and how it will continue to shape our world.
    <br/><br/>
    ¶¶¶¶¶
    
    computing / history / impact / PC<br/>
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    <br/><br/><br/><br/>
    
    A TRIBUTE TO THE VISIONARIES<br/>
    
    4/10/2023
    ·
    
    Opinions, Reviews<br/><br/>
    The Pioneers of Personal Computing: A Tribute to the Visionaries
    <br/>
    Personal computing has revolutionized the way we live, work, and communicate. But who were the visionaries who made it all possible? In this post, we’ll take a look back at some of the pioneers of personal computing and their contributions to this field.
    <br/>
    STEVE WOZNIAK AND STEVE JOBS:
    The founders of Apple Computer, Inc. Wozniak designed the hardware for the Apple I and II computers, while Jobs focused on marketing and design. Together, they created the first successful personal computer and laid the foundation for the modern computing industry.
    <br/><br/>
    BILL GATES AND PAUL ALLEN:<br/>
    The founders of Microsoft Corporation. Gates and Allen developed the first BASIC interpreter for the Altair 8800 computer and went on to create the most popular operating system of the 1990s, Windows. Microsoft also developed popular productivity software such as Word and Excel, which revolutionized the way we work.
    <br/><br/>
    GARY KILDALL:<br/>
    The creator of CP/M, the first popular operating system for personal computers. Kildall’s operating system was used on many early personal computers, including the popular Commodore 64. CP/M paved the way for the widespread adoption of personal computers in the 1980s.
    <br/><br/>
    LEE FELSENSTEIN:<br/>
    The designer of the first mass-produced personal computer, the Osborne 1. Felsenstein was also a key figure in the development of the Homebrew Computer Club, which brought together hobbyists and enthusiasts interested in personal computing.
    <br/><br/>
    TIM BERNERS-LEE:<br/>
    The inventor of the World Wide Web. Berners-Lee developed the first web browser and the protocols that allowed computers to communicate over the internet. His invention has transformed the way we access and share information.
    <br/>
    These pioneers of personal computing were not just technologists, they were also entrepreneurs and innovators who saw the potential of personal computing before anyone else did. They were driven by a passion for technology and a desire to make computing accessible to everyone. Their contributions have paved the way for a world in which personal computing is an essential part of our daily lives.
    <br/>
    Today, we continue to see the impact of their work in the many new technologies that have emerged, such as cloud computing, artificial intelligence, and the internet of things. The pioneers of personal computing have left a lasting legacy that will continue to shape the future of technology for years to come.
    <br/><br/>
    ¶¶¶¶¶
    
    computing / history / impact / PC / tribute<br/>
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    <br/><br/><br/><br/>
    
    THE EVOLUTION OF PCS<br/>
    
    4/10/2023
    ·
    
    Reviews<br/><br/>
    From Hobbyists to Mainstream: The Evolution of Personal Computing
    <br/>
    Personal computing has come a long way since the 1970s when the first personal computers were built by hobbyists and enthusiasts. Back then, personal computers were not taken seriously by businesses or the mainstream public. Today, personal computers are an essential part of our daily lives, used for work, communication, entertainment, and much more.
    <br/>
    The early personal computers, such as the Altair 8800 and the Apple I, were build-it-yourself kits that came with no software or peripherals. They were primarily used by hobbyists and enthusiasts who wanted to experiment with computers. These early computers were not very powerful, and they were not easy to use. However, they were a breakthrough in their own right, as they showed that computers could be small and affordable enough for individuals to buy and experiment with.
    <br/>
    In the late 1970s and early 1980s, personal computers started to become more mainstream. Companies like Apple, Commodore, and Tandy started to release personal computers that were pre-assembled, easy to use, and came with software and peripherals. This made personal computers more accessible to non-experts, and it opened up new opportunities for innovation and creativity.
    <br/>
    In the mid-1980s, personal computing really took off with the introduction of the IBM PC and compatible computers. These computers were more powerful and versatile than earlier models, and they were designed for business and government use. However, they were also affordable enough for individuals to buy and use at home.
    <br/>
    The graphical user interface (GUI) was another key development that helped to make personal computing more accessible. The GUI was first developed by Xerox PARC in the 1970s, but it was popularized by Apple with the release of the Macintosh in 1984. The GUI made personal computers much more user-friendly and accessible to non-experts.
    <br/>
    The rise of the internet in the 1990s and 2000s also had a huge impact on personal computing. The internet made it possible to connect personal computers together, and it allowed people to access information and services from anywhere in the world. The internet also gave rise to new types of software, such as web browsers and email clients.
    <br/>
    Today, personal computing is more powerful, versatile, and accessible than ever before. We have laptops, tablets, and smartphones that are designed for personal use, as well as powerful desktop computers that are used for work and gaming. We have software and services that allow us to do just about anything, from video editing to online shopping. Personal computing has truly come a long way, from a hobbyist pursuit to an essential part of our daily lives.
    <br/><br/>
    ¶¶¶¶¶
    
    computing / history / impact / PC<br/>
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    <br/><br/><br/><br/>
    
    THE DOMINANT OS OF THE PC ERA<br/>
    
    4/10/2023
    ·
    
    Opinions<br/><br/>
    How DOS Became the Dominant Operating System of the PC Era
    <br/>
    MS-DOS, short for Microsoft Disk Operating System, was the operating system that dominated the personal computer era from the early 1980s to the mid-1990s. MS-DOS was created by Microsoft and was initially used on IBM PC and compatible computers. But how did MS-DOS become the dominant operating system of the PC era?
    <br/>
    One of the main reasons for MS-DOS’s success was its availability. IBM selected MS-DOS as the operating system for its IBM PC in 1981, and this gave Microsoft a huge boost. Other computer manufacturers started to license MS-DOS, which allowed it to become widely available. As more people bought personal computers, they were likely to choose MS-DOS because it was the operating system they were already familiar with.
    <br/>
    Another reason for MS-DOS’s success was its simplicity. MS-DOS was a command-line operating system, which meant that users had to type commands to perform tasks. This was very different from the graphical user interface (GUI) of the Apple Macintosh, which was released around the same time. However, the command-line interface had advantages. It was faster and more efficient than a GUI, and it was also easier for developers to create software for MS-DOS.
    <br/>
    MS-DOS was also very versatile. It could run on a wide range of hardware, which made it attractive to computer manufacturers. It also had a relatively small footprint, which meant that it could run on computers with limited memory and processing power.
    <br/>
    Another reason for MS-DOS’s success was the availability of software. MS-DOS had a large library of software that was available for it, including word processors, spreadsheets, and games. This made it attractive to users who wanted to perform a wide range of tasks on their personal computers.
    <br/>
    Finally, Microsoft’s business strategy also played a role in MS-DOS’s dominance. Microsoft licensed MS-DOS to computer manufacturers for a fee, and it also sold MS-DOS to end-users. This allowed Microsoft to make money from both the hardware and software sides of the personal computer industry.
    <br/>
    In conclusion, MS-DOS became the dominant operating system of the PC era because of its availability, simplicity, versatility, software library, and Microsoft’s business strategy. While MS-DOS has since been replaced by newer operating systems, its impact on the personal computer industry cannot be overstated.
    <br/><br/>
    ¶¶¶¶¶
    
    computing / history / legacy / PC / PC era<br/>
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    <br/><br/><br/><br/>
    
    THE BIRTH OF PERSONAL COMPUTERS<br/>
    
    4/10/2023
    ·
    
    Reviews<br/><br/>
    The Birth of Personal Computers: A Look Back at the Revolution
    <br/>
    The personal computer (PC) revolution began in the 1970s and 1980s, when computers became smaller, more affordable, and more accessible to the average person. Before the PC, computers were large, expensive, and used primarily by businesses, governments, and universities.
    <br/>
    The first personal computer, the Altair 8800, was released in 1975. It was a build-it-yourself kit that came with no software or peripherals, but it was a breakthrough in its own right. The Altair 8800 was the first computer that was small and affordable enough for hobbyists and enthusiasts to buy and experiment with.
    <br/>
    The Altair 8800 was followed by the Apple I in 1976 and the Apple II in 1977. These were the first personal computers that came with a keyboard, monitor, and pre-installed software. They were also the first personal computers that were easy enough to use that non-experts could operate them.
    <br/>
    In the early 1980s, IBM released the IBM PC, which set a new standard for personal computers. It was more powerful and versatile than earlier models, and it was the first personal computer that was widely adopted by businesses and governments. It also introduced the concept of an “open architecture,” which meant that other companies could create compatible hardware and software.
    <br/>
    One of the key factors that contributed to the success of personal computers was the development of the graphical user interface (GUI). This was pioneered by Xerox PARC in the 1970s and popularized by Apple in the early 1980s with the Macintosh. The GUI made personal computers much more user-friendly and accessible to non-experts.
    <br/>
    Another key factor was the development of software that was designed specifically for personal computers. The first operating system for personal computers was CP/M, which was created in 1974 by Gary Kildall. However, the operating system that really took off was MS-DOS, which was created by Microsoft in 1981. MS-DOS was the dominant operating system for personal computers for over a decade, until it was eventually replaced by Microsoft Windows.
    <br/>
    The birth of personal computers was a revolution that transformed the way people work, communicate, and live. It made computing power accessible to everyone, not just large corporations and governments. It opened up new opportunities for innovation and creativity, and it paved the way for the digital age that we live in today.
    <br/><br/>
    ¶¶¶¶¶
    
    birth / computing / history / impact / PC<br/>
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    
    ¶¶¶¶¶
    <br/><br/><br/><br/>
        `,
        `
    ABOUT <br/>
    4/10/2023
    ·
    <br/><br/>
    DOS is a blog theme designed for the nostalgic ones — as a tribute to the folks that invented computing as we know it today. Let us blog as if we were back to green (or amber) phosphor back again.
    <br/><br/>
    WHO WE ARE<br/>
    <strong>Suggested text:</strong> Our website address is: https://dos.mystagingwebsite.com.
    <br/><br/>
    COMMENTS<br/>
    Suggested text: When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection.
    <br/>
    An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.
    <br/><br/>
    MEDIA<br/>
    Suggested text: If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.
    <br/><br/>
    COOKIES<br/>
    Suggested text: If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.
    <br/>
    If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.
    <br/>
    When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select “Remember Me”, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.
    <br/>
    If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.
    <br/><br/>
    EMBEDDED CONTENT FROM OTHER WEBSITES<br/>
    Suggested text: Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.
    <br/>
    These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.
    <br/>
    WHO WE SHARE YOUR DATA WITH
    Suggested text: If you request a password reset, your IP address will be included in the reset email.
    <br/><br/>
    HOW LONG WE RETAIN YOUR DATA<br/>
    Suggested text: If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.
    <br/>
    For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.
    <br/><br/>
    WHAT RIGHTS YOU HAVE OVER YOUR DATA<br/>
    Suggested text: If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.
    <br/><br/>
    WHERE YOUR DATA IS SENT<br/>
    Suggested text: Visitor comments may be checked through an automated spam detection service.<br/><br/>
    `,
        `
        
THE LEGACY OF DOS
<br/>
How the First Operating System Shaped the Future of Computing When we think about the history of personal computing, it’s impossible to ignore the role of DOS. The Disk Operating System, created by Microsoft in the early 1980s, was the first widely used operating system for IBM-compatible personal computers. In this post, we’ll take a […]
<br/>
4/10/2023
·

Reviews
<br/><br/>
¶¶¶¶¶

¶¶¶¶¶
<br/><br/><br/><br/>

A TRIBUTE TO THE VISIONARIES<br/>

The Pioneers of Personal Computing: A Tribute to the Visionaries Personal computing has revolutionized the way we live, work, and communicate. But who were the visionaries who made it all possible? In this post, we’ll take a look back at some of the pioneers of personal computing and their contributions to this field. Steve Wozniak […]
<br/>
4/10/2023
·

Opinions, Reviews<br/><br/>
¶¶¶¶¶

¶¶¶¶¶
<br/><br/><br/><br/>
THE EVOLUTION OF PCS<br/>

From Hobbyists to Mainstream: The Evolution of Personal Computing Personal computing has come a long way since the 1970s when the first personal computers were built by hobbyists and enthusiasts. Back then, personal computers were not taken seriously by businesses or the mainstream public. Today, personal computers are an essential part of our daily lives, […]
<br/>
4/10/2023
·

Reviews<br/><br/>
¶¶¶¶¶

¶¶¶¶¶<br/><br/><br/><br/>

THE BIRTH OF PERSONAL COMPUTERS<br/>

The Birth of Personal Computers: A Look Back at the Revolution The personal computer (PC) revolution began in the 1970s and 1980s, when computers became smaller, more affordable, and more accessible to the average person. Before the PC, computers were large, expensive, and used primarily by businesses, governments, and universities. The first personal computer, the […]
<br/>
4/10/2023
·

Reviews<br/>
¶¶¶¶¶

¶¶¶¶¶<br/><br/><br/><br/>
    `
    ]
    const [text, setText] = React.useState(contents[0])
    console.log(text)
    return (
        <Container>
            <Header>
                <BetweenBox>
                    <TitleBlog>DOS ATTACKING</TitleBlog>
                    <Avatar src={avatar} size={40} />
                </BetweenBox>
                <BetweenBox>
                    <div>Here's to the nostalgic ones! A tribute to the folks that</div>
                    <Tab onClick={() => setText(contents[0])}># THE LEGACY</Tab>
                </BetweenBox>
                <BetweenBox>
                    <div>invented the Computing we know today.</div>
                    <Tab onClick={() => setText(contents[1])}># ABOUT</Tab>
                </BetweenBox>
                <BetweenBox>
                    <div></div>
                    <Tab onClick={() => setText(contents[2])}># REVIEWS</Tab>
                </BetweenBox>
            </Header>
            <Body>
                <td dangerouslySetInnerHTML={{ __html: text }} />
            </Body>
            <Fotter>
                <div>@Blog at WordPress.com.</div>
                <div>TUMBLR / DAYONE / INSTAGRAM</div>
            </Fotter>
        </Container>
    )
}

const Container = styled(Row)`
    background-color: #1a1a1a;
    color: #33ff10;
    position: relative;
    font-size: 22px;
`
const Header = styled.div`
    background-color: #1a1a1a;
    z-index: 30;
    position: fixed;
    top: 56px;
    left: 18%;
    right: 24px;
    padding-top: 12px;
    border-bottom: 1px dashed #33ff10;
`
const Body = styled(Col)`
    padding-top: 240px;
    margin: 0 24px;
    width: 60%;
    font-size: 18px;
    letter-spacing: .8px;
`
const BetweenBox = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 4px 0;
`
const TitleBlog = styled.div`
    padding: 12px 18px;
    font-size: 26px;
    font-weight: 600;

`
const Fotter = styled.div`
    width: 100%;
    margin: 0 24px;
    display: flex;
    justify-content: space-between;
    padding: 32px 0;
    border-top: 1px dashed #33ff10;
`
const Tab = styled.div`
    &:hover{
        cursor: pointer;
    }
`
export default Blog
