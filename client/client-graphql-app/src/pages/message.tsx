import * as React from 'react'
import { Col, Row, Input, Avatar } from "antd"
import styled from "styled-components"
import type { SearchProps } from 'antd/es/input';

const { Search } = Input;
const Messages = () => {
    React.useEffect(() => { }, [])
    const onSearch: SearchProps['onSearch'] = () => {

    }
    return (
        <Container>
            <MessageBlock>
                <Col span={7}>
                    <ListBox span={7}>
                        <Search placeholder="search for friends..." onSearch={onSearch} />
                        <ContactCard>
                            <AvatarBox>
                                <Avatar />
                                <ActiveDot />
                            </AvatarBox>
                            <ContactLater>
                                <ContactName>Tuan Anh Dinh</ContactName>
                                <ContactCurrent>Hey, we have meet at 7h30 pm</ContactCurrent>
                            </ContactLater>
                            <ContactInfo>
                                <ContactTime>11:26</ContactTime>
                                <UnreadMessage>1</UnreadMessage>
                            </ContactInfo>
                        </ContactCard>
                    </ListBox>
                </Col>
                <Col span={17}>
                    <MessageBox span={17}>ok2</MessageBox>
                </Col>
            </MessageBlock>
            <SlideIn></SlideIn>
        </Container>
    )
}

const Container = styled(Row)`
    
`
const MessageBlock = styled(Row)`
    width: 100%;
    margin-right: 24px;
    background-color: #f9fafb;
    border-radius: 24px;
    padding: 24px 0;
    min-height: 700px;
`
const ListBox = styled.div`
    background-color: #fff;
    width: 84%;
    margin: 0 auto;
    min-height: 660px;
    padding: 12px;
    border-radius: 24px;
`
const ContactCard = styled.div`

`
const ContactLater = styled.div`

`
const ContactName = styled.div`

`
const ContactCurrent = styled.div`

`
const AvatarBox = styled.div`

`
const ActiveDot = styled.div`

`
const ContactInfo = styled.div`

`
const ContactTime = styled.div`

`
const UnreadMessage = styled.div`

`
const MessageBox = styled.div`
    background-color: #fff;
    width: 92%;
    margin: 0 auto;
    min-height: 660px;
    border-radius: 24px;
`
const SlideIn = styled(Row)`
`
export default Messages
