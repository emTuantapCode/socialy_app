import styled from "styled-components"
import { Row, Col } from "antd"

const Community = () => {
    return (
        <>
            <Intro>Let's join our community for helping Yuna people</Intro>
            <Container>
                <Title span={4}>Yua Childrent</Title>
                <Box span={19}>
                    <StyledIframe src="https://demo.anarieldesign.com/yuna-children/" frameBorder="0"></StyledIframe>
                </Box>
            </Container>
            <Container>
                <Box span={20}>
                    <StyledIframe src="https://demo.anarieldesign.com/yuna-nature/" frameBorder="0"></StyledIframe>
                </Box>
                <Title span={4}>Yua Nature</Title>
            </Container>
            <Container>
                <Title span={4}>Yua Sanctuary</Title>
                <Box span={19}>
                    <StyledIframe src="https://demo.anarieldesign.com/yuna-sanctuary/" frameBorder="0"></StyledIframe>
                </Box>
            </Container>
        </>
    )
}
const Box = styled(Col)`
    height: 400px;
    border-radius: 12px;
    overflow: hidden;
`
const StyledIframe = styled.iframe`
    width: 102%;
    height: 400px;
`
const Intro = styled.div`
    width: 100%;
    text-align: center;
    padding: 24px 12px;
    font-size: 26px;
    font-weight: 800;
`
const Container = styled(Row)`
    margin-top: 32px;
    margin-bottom: 82px;
`
const Title = styled(Col)`
    font-size: 22px;
    font-weight: 600;
    padding: 0 8px;
    display: flex;
    align-items: center;
`
export default Community
