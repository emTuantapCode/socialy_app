import * as React from 'react'
import styled, { css } from 'styled-components'
import { UserProps } from '../../types/user'
import { Avatar } from 'antd'

const FriendCard: React.FC<UserProps> = ({ data }) => {
    return (
        <Container>
            <Avatar size={40} src={data?.avatar?.src} />
            <StyledName>{data?.name}</StyledName>
            <Dot $actived={true} />
        </Container>
    )
}

const Container = styled.div`
    width: full;
    display: flex;
    padding: 4px 0;
    align-items: center;
`
const StyledName = styled.div`
    padding: 0 8%;
    color: #4e5d78;
    font-size: 16px;
    font-weight: 500;
    min-width: 80%;
`
const Dot = styled.div`
    width: 8px;
    height: 8px;
    border-radius: 50%;
    ${(props) => {
        //@ts-ignore
        if (props.$actived) return css`
        background-color: #38cb89;
        box-shadow: 0 0 4px #38cb89;
        `
        else return css`
        background-color: #ccc;
        box-shadow: 0 0 4px #ccc;
        `
    }}
`

export default FriendCard
