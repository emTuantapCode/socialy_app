import * as React from 'react'
import styled, { css } from "styled-components"
import { useMutation } from '@apollo/client';
import { Row, Col, Avatar, Input, Divider, Popover } from "antd"
import { FcGlobe } from 'react-icons/fc';
import type { PostComponent, Media } from "../../types/post";
import { BsChatText, BsFillHeartFill } from 'react-icons/bs'
import { BiShareAlt } from 'react-icons/bi'
import { RiDeleteBin6Line } from 'react-icons/ri'
import { useSelector } from "react-redux";
import { AiOutlineSend, AiOutlineSmile } from 'react-icons/ai'
import type { FixTypeLater } from "react-redux";
import type { EmojiClickData } from 'emoji-picker-react';
import EmojiPicker from "emoji-picker-react";
import { CREATE_Comment, DELETE_Comment, FAVORITE_Comment } from '../../apis/comment';
import { FAVORITE_Post } from '../../apis/post'
import formatTimeAgo from '../../constants/helpers/format_time';

const { TextArea } = Input;
const Post: React.FC<PostComponent> = ({ post }) => {
    const { user } = useSelector((state: FixTypeLater) => state.user)
    const [comment, setComment] = React.useState<string>('')
    const [emoji, setEmoji] = React.useState('')
    const [comments, setComments] = React.useState(post?.comment)
    const [commentNumber, setCommentNumber] = React.useState(post?.commentNumber)
    const [favorited, setFavorited] = React.useState(post?.favorited)
    const [favoritedNumber, setFavoritedNumber] = React.useState(post?.favoriteNumber)
    const [favoriters, setFavoriters] = React.useState(post?.favoriters)
    const [createComment] = useMutation(CREATE_Comment, {
        onCompleted: (data) => {
            setComments(data.createComment.post?.comment)
            setCommentNumber(data.createComment.post?.commentNumber)
        }
    });
    const [deleteComment] = useMutation(DELETE_Comment, {
        onCompleted: (data) => {
            setComments(data.deleteComment.post?.comment)
            setCommentNumber(data.deleteComment.post?.commentNumber)
        }
    });
    const [changeFavoriteComment] = useMutation(FAVORITE_Comment, {
        onCompleted: (data) => {
            setComments(data.changeFavoriteComment.post?.comment)
        }
    });
    const [changeFavoritePost] = useMutation(FAVORITE_Post, {
        onCompleted: (data) => {
            setFavorited(data.changeFavoritePost.favorited)
            setFavoritedNumber(data.changeFavoritePost.favoriteNumber)
            setFavoriters(data.changeFavoritePost.favoriters)
        }
    });
    const RenderLayout = (media: Media[] | []) => {
        switch (media.length) {
            case 0:
                return <></>
            case 1:
                return (
                    <RoundedContainer>
                        <SigleImg src={media[0]?.mediasrc} alt={media[0]?.filename} />
                    </RoundedContainer>
                )
            case 2:
                return (
                    <RoundedContainer>
                        <DoubleImg src={media[0]?.mediasrc} alt={media[0]?.filename} />
                        <DoubleImg src={media[1]?.mediasrc} alt={media[1]?.filename} />
                    </RoundedContainer>
                )
            case 3:
                return (
                    <RoundedContainer>
                        <DoubleImg src={media[0]?.mediasrc} alt={media[0]?.filename} />
                        <DoubleImg src={media[1]?.mediasrc} alt={media[1]?.filename} />
                        <SigleImg src={media[2]?.mediasrc} alt={media[2]?.filename} />
                    </RoundedContainer>
                )
            case 4:
                return (
                    <RoundedContainer>
                        <DoubleImg src={media[0]?.mediasrc} alt={media[0]?.filename} />
                        <DoubleImg src={media[1]?.mediasrc} alt={media[1]?.filename} />
                        <DoubleImg src={media[2]?.mediasrc} alt={media[2]?.filename} />
                        <DoubleImg src={media[3]?.mediasrc} alt={media[3]?.filename} />
                    </RoundedContainer>
                )
            default:
                return (
                    <RoundedContainer>
                        <DoubleImg src={media[0]?.mediasrc} alt={media[0]?.filename} />
                        <DoubleImg src={media[1]?.mediasrc} alt={media[1]?.filename} />
                        <DoubleImg src={media[2]?.mediasrc} alt={media[2]?.filename} />
                        <DoubleImg src={media[3]?.mediasrc} alt={media[3]?.filename} />
                        <MoreImg>+{media.length - 4}</MoreImg>
                    </RoundedContainer>
                )
        }
    }
    React.useEffect(() => {
        setComment(preve => preve + emoji)
    }, [emoji])
    const onEmojiClick = (emoji: EmojiClickData) => setEmoji(emoji.emoji)
    const handleSubmit = () => {
        createComment({
            variables: {
                data: {
                    post: post?._id,
                    content: comment
                }
            }
        })
        setComment('')
    }
    const handleDeleteCommet = (_id: string | undefined) => deleteComment({
        variables: {
            _id: _id
        }
    })
    const handleFavoriteComment = (_id: string | undefined) => changeFavoriteComment({
        variables: {
            _id: _id
        }
    })
    const handleFavoritePost = (_id: string | undefined) => changeFavoritePost({
        variables: {
            _id: _id
        }
    })
    return (
        <Container>
            <HeadBox>
                <Col><StyledAvatar size={46} src={post?.author?.avatar?.src} /></Col>
                <Col>
                    <UserName>{post?.author?.name}</UserName>
                    <p>public <FcGlobe /></p>
                </Col>
                <Felling>{post?.felling}</Felling>
            </HeadBox>
            <StyledInput autoSize customColor={post?.color} value={post?.content} bordered={false} />
            {
                //@ts-ignore
                RenderLayout(post?.media)
            }
            <SpaceBetween>
                <Col>
                    <AvatarGroup maxCount={4}>
                        {favoriters?.map(element => <Avatar src={element.avatar?.src} />)}
                    </AvatarGroup>
                </Col>
                <Col><span style={{ padding: '0 16px' }}>{favoritedNumber} Favorite</span> <span>{commentNumber} Comments</span></Col>
            </SpaceBetween>
            <Divider style={{ margin: 0 }} />
            <SpaceBetween>
                <StyledFavorite loved={favorited} onClick={() => handleFavoritePost(post?._id)}><BsFillHeartFill /> favorited</StyledFavorite>
                <StyledButton><BsChatText color="#38cb89" /> Comment</StyledButton>
                <StyledButton><BiShareAlt color="#1677ff" />Share</StyledButton>
            </SpaceBetween>
            <Divider style={{ margin: 0 }} />
            {comments?.length !== 0 && comments?.map((comment) => (
                <CommentContainer>
                    <Avatar src={comment?.author?.avatar?.src} />
                    <Col>
                        <CommentContent>
                            <div style={{ fontSize: '16px', fontWeight: '500' }}>{comment?.author?.name}</div>
                            <div>{comment?.content}</div>
                        </CommentContent>
                        <CommentTrigger>
                            <StyledFavorite loved={comment?.favorite} onClick={() => handleFavoriteComment(comment?._id)}>
                                <BsFillHeartFill />
                            </StyledFavorite>
                            <div>{comment?.favoriteNumber} like</div>
                            <div>{formatTimeAgo(comment?.createdAt)}</div>
                        </CommentTrigger>
                    </Col>
                    {(user?._id === comment?.author?._id) && <DeleteComment color={'#ff5630'} onClick={() => handleDeleteCommet(comment?._id)} />}
                </CommentContainer>
            ))}
            <SpaceBetween>
                <Avatar src={user?.avatar?.src} size={36} />
                <CommentBox span={18}>
                    <StyledInput
                        bordered={false}
                        autoSize
                        placeholder={`Write a comment...`}
                        value={comment}
                        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setComment(e.target.value)}
                    />
                    <Popover content={
                        //@ts-ignore
                        <EmojiPicker onEmojiClick={onEmojiClick} emojiStyle={'native'} height={350} searchDisabled={true} />
                    } title="Emoji picker" trigger="click">
                        <AiOutlineSmile size={24} />
                    </Popover>
                </CommentBox>
                <SentBox onClick={() => handleSubmit()}><AiOutlineSend size={22} color="#1677ff" /></SentBox>
            </SpaceBetween>
        </Container>
    )
}

const Container = styled(Row)`
    padding: 12px;
    background-color: #fff;
    margin: 24px 0;
    border-radius: 12px;
`
const HeadBox = styled(Row)`
    margin: 12px 0;
    color: #4e5d78;
`
const Felling = styled(Col)`
    font-size: 16px;
    margin-left: 12px;
`
const StyledAvatar = styled(Avatar)`
    border: solid 1px #ccc;
    margin-right: 12px;
`
const UserName = styled.div`
    font-size: 16px;
    font-weight: 600;
`
const StyledInput = styled(TextArea)`
    padding: 0;
    font-size: 16px;
    color: ${(props) => props.customColor};
    text-decoration: none;
    user-select: none;
`
const RoundedContainer = styled(Row)`
    border-radius: 8px;
    overflow: hidden;
    display: flex;
    align-items: stretch;
    position: relative;
`
const DoubleImg = styled.img`
    object-fit: cover;
    width: 50%;
    height: full;
`
const SigleImg = styled.img`
    object-fit: cover;
    width: 100%;
`
const MoreImg = styled.div`
    position: absolute;
    bottom: 20%;
    right: 22%;
    font-size: 40px;
    font-weight: 700;
    color: #fff;
    text-shadow: 2px 2px 8px #333;
`
const AvatarGroup = styled(Avatar.Group)`
    color: '#f56a00';
    backgroundColor: '#fde3cf';
    margin: 12px 0;
`
const SpaceBetween = styled(Row)`
    display: flex;
    justify-content: space-between;
    width: 100%;
    align-items: center;
    min-height: 36px;
`
const StyledButton = styled(Col)`
    font-size: 18px;
    font-weight: 500;
    &:hover{
        cursor: pointer;
    }
`
const StyledFavorite = styled(StyledButton)`
    &:hover{
        cursor: pointer;
    }
    ${(props) => {
        //@ts-ignore
        if (props.loved) return css`
    color: #ff5630;
    `
    }}
`
const CommentBox = styled(Col)`
    display: flex;
    background-color: #f9fafb;
    margin: 8px 0;
    padding: 2px 8px;
    border-radius: 12px;
    align-items: center;
`
const SentBox = styled(Col)`
    background-color: #ebf2ff;
    padding: 5px 6px;
    border-radius: 8px;
    &:hover{
        cursor: pointer;
    }
`
const CommentContainer = styled(Row)`
    position: relative;
    max-width: 100%;
    min-width: 60%;
    margin: 12px 0;
    align-items: center;
`
const DeleteComment = styled(RiDeleteBin6Line)`
    &:hover{
        cursor: pointer;
    }
    
`
const CommentContent = styled.div`
    padding: 8px 12px;
    background-color: #f9fafb;
    margin-left: 12px;
    border-radius: 12px;
`
const CommentTrigger = styled(Row)`
    justify-content: space-between;
    margin-left: 22px; 
`
export default Post
