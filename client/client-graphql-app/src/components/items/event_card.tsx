import { Avatar, Col, Row, Dropdown, Button, Input, Card } from "antd"
import {
    AiOutlineCamera, AiOutlineFacebook, AiOutlineGoogle, AiOutlineInstagram, AiOutlineSend, AiOutlineTwitter
} from 'react-icons/ai'
import { BiLogoTiktok } from 'react-icons/bi'
import { BsBookHalf, BsGlobeAsiaAustralia } from 'react-icons/bs'
import styled from "styled-components"
import type { MenuProps } from 'antd'

const items: MenuProps['items'] = [
    {
        key: '1',
        label: (
            <p>
                tuananhd.020302@gmail.com
            </p>
        ),
    }
];
const EventCard = () => {
    return (
        <CenterCol>
            <StyledCard headStyle={{ padding: '0 12px', color: '#4e5d78' }} bodyStyle={{ padding: '12px', color: '#4e5d78' }} title="You Might Like" bordered={false} extra={<a href="https://www.topcv.vn/xem-cv/UlRRBldWUwcABlEAWFQJVVsBBANcB1dSAFUCAw31e9">See All</a>}>
                <Row>
                    <Col><Avatar size={50} src={'https://res.cloudinary.com/dciozzifg/image/upload/w_300,h_320/coppyImg_pddp4q.jpg'} /></Col>
                    <Col style={{ marginLeft: '8px' }}>
                        <div style={{ fontWeight: '600' }}>Dinh Tuan Anh - Alan</div>
                        <p style={{ fontSize: '12px', color: '#ccc' }}>fullstack web developer</p>
                        <StyledRow>
                            <BsGlobeAsiaAustralia />
                            <AiOutlineGoogle />
                            <AiOutlineFacebook />
                            <AiOutlineTwitter />
                            <AiOutlineInstagram />
                            <BiLogoTiktok />
                        </StyledRow>
                    </Col>
                </Row>
                <StyledRow>
                    <Dropdown menu={{ items }} placement="bottomLeft" arrow>
                        <Button>Contact</Button>
                    </Dropdown>
                    <Button type="primary">
                        <a href="https://www.facebook.com/anh.tuan.020302">Follow</a>
                    </Button>
                </StyledRow>
            </StyledCard>
            <StyledCard headStyle={{ padding: '0 12px', color: '#4e5d78' }} bodyStyle={{ padding: '12px', color: '#4e5d78' }} title="Recent Event" bordered={false} extra={<p>...</p>}>
                <Row style={{ backgroundColor: '#f9fafb', padding: '12px', borderRadius: '8px', marginBottom: '16px' }}>
                    <Col style={{ padding: '8px 12px', backgroundColor: '#ffddd6', borderRadius: '4px', display: 'flex', alignItems: 'center' }}><AiOutlineCamera size={24} color="#ff5630" /></Col>
                    <Col style={{ marginLeft: '8px' }} >
                        <div style={{ fontWeight: '600' }}>Photography Ideas</div>
                        <p style={{ fontSize: '12px', color: '#ccc' }}>Reflections. Reflections <br /> work because they...</p>
                    </Col>
                </Row>
                <Row style={{ backgroundColor: '#f9fafb', padding: '12px', borderRadius: '8px' }}>
                    <Col style={{ padding: '8px 12px', backgroundColor: '#d7f5e7', borderRadius: '4px', display: 'flex', alignItems: 'center' }}><BsBookHalf size={24} color="#38cb89" /></Col>
                    <Col style={{ marginLeft: '8px' }} >
                        <div style={{ fontWeight: '600' }}>Graduation Ceremony</div>
                        <p style={{ fontSize: '12px', color: '#ccc' }}>The graduation ceremony is<br /> also sometimes called...</p>
                    </Col>
                </Row>
            </StyledCard>
            <StyledCard headStyle={{ padding: '0 12px', color: '#4e5d78' }} bodyStyle={{ padding: '12px', color: '#4e5d78' }} title="Birthdays" bordered={false} extra={<a href="#">See All</a>}>
                <Row>
                    <Col><Avatar size={50} src={'https://plus.unsplash.com/premium_photo-1677545182425-4fb12bdb9faf?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8N3x8Y2F0JTIwZmFjZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60'} /></Col>
                    <Col style={{ marginLeft: '8px' }}>
                        <div style={{ fontWeight: '600' }}>Alan Cat</div>
                        <p style={{ fontSize: '12px', color: '#ccc' }}>Birthday on March 2</p>
                    </Col>
                </Row>
                <Input style={{ marginTop: '12px' }} addonAfter={<AiOutlineSend color="#1677ff" />} placeholder="Upcomming feature !" />
            </StyledCard>
        </CenterCol>
    )
}

const CenterCol = styled(Col)`
    width: 90%;
    margin: 0 auto;
`
const StyledRow = styled(Row)`
    display: flex;
    justify-content: space-between;
    margin-top: 16px;
`
const StyledCard = styled(Card)`
    margin-bottom: 32px;
`

export default EventCard
