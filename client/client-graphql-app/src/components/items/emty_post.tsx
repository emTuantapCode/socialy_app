import styled from "styled-components"
import EmtyImg from '../../assets/component/EmtyData.png'
import EnrichFeed from '../../assets/component/EnrichFeed.png'

const EmtyPost = () => {
    return (
        <Container>
            <StyledText>It's happy to see guy here! make friend for enrich your newsfeed 🫡 <br /> Notice your friends request !</StyledText>
            <StyledImg src={EnrichFeed} alt="emty_data" />
            <BoxImg>
                <StyledImg src={EmtyImg} alt="emty_data" />
            </BoxImg>
        </Container>
    )
}

const Container = styled.div`
    padding: 12px;
    background-color: #fff;
    border-radius: 12px;
`
const StyledText = styled.div`
    font-size: 18px;
    font-weight: 500;
`
const BoxImg = styled.div`
    background-color: #000;
    border-radius: 12px;
`
const StyledImg = styled.img`
    width: 100%;
    border-radius: 12px;
`

export default EmtyPost
