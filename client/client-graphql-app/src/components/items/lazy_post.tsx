import styled from "styled-components"
import { Skeleton, Row, Col } from "antd"

const LazyPost = () => {
    return (
        <Container>
            <Skeleton loading={true} active avatar paragraph={{ rows: 3 }}></Skeleton>
            <Row>
                <StyledImg span={12}>
                    <Skeleton.Image style={{ width: '200px', height: '200px' }} active />
                </StyledImg>
                <StyledImg span={12}>
                    <Skeleton.Image style={{ width: '200px', height: '200px' }} active />
                </StyledImg>
            </Row>
            <Row>
                <StyledImg span={12}>
                    <Skeleton.Image style={{ width: '200px', height: '200px' }} active />
                </StyledImg>
                <StyledImg span={12}>
                    <Skeleton.Image style={{ width: '200px', height: '200px' }} active />
                </StyledImg>
            </Row>
        </Container>
    )
}

const Container = styled.div`
    background-color: #fff;
    padding: 12px;
    margin: 24px 0;
    border-radius: 12px;
`
const StyledImg = styled(Col)`
    display: flex;
    justify-content: center;
    margin: 12px 0;
`

export default LazyPost
