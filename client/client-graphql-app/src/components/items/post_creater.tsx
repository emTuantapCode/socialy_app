import * as React from 'react';
import { useMutation } from '@apollo/client';
import { Modal, Row, Col, Avatar, ColorPicker, Popover, Button, Upload, Input, Dropdown, message } from "antd";
import { PostProps } from '../../types/post';
import styled, { css } from 'styled-components';
import type { FixTypeLater } from "react-redux";
import type { Color } from 'antd/es/color-picker';
import { useSelector } from "react-redux";
import { FcGlobe, FcImageFile } from 'react-icons/fc';
import { TbMoodHappy } from 'react-icons/tb';
import { HiMiniPhoto } from 'react-icons/hi2';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import { BsFillEmojiWinkFill, BsFillCameraVideoFill, BsFillTagsFill } from 'react-icons/bs';
import { FaLocationDot } from 'react-icons/fa6'
import EmojiPicker from 'emoji-picker-react';
import { UploadRequestOption as RcCustomRequestOptions } from 'rc-upload/lib/interface'
import ColorSelect from '../../assets/component/SATP_Aa_square-2x.png'
import { EmtyLayout, SigleLayout, DoubleLayout, TripleLayout, QuatarLayout, MultipleLayout } from '../layouts/post_layout';
import type { EmojiClickData, EmojiStyle } from 'emoji-picker-react';
import type { MenuProps } from 'antd';
import { RcFile } from 'antd/lib/upload/interface'
import { Cloud } from '../../apis/cloud';
import { CREATE_Post } from '../../apis/post';

//@ts-ignore
const EMOJITYPE: EmojiStyle = 'native'
const items: MenuProps['items'] = [
    {
        key: '😊happy',
        label: (
            <p>😊happy</p>
        ),
    },
    {
        key: '😞sad',
        label: (
            <p>😞sad</p>
        ),
    },
    {
        key: '😁interesting',
        label: (
            <p>😁interesting</p>
        ),
    },
    {
        key: '🍀fortune',
        label: (
            <p>🍀fortune</p>
        ),
    }
];

const { TextArea } = Input;
const PostCreater: React.FC<PostProps> = ({ open, handleCancel }) => {
    const { user } = useSelector((state: FixTypeLater) => state.user)
    const [color, setColor] = React.useState<Color | string>('#000')
    const [preview, setPreview] = React.useState(false)
    const [files, setFiles] = React.useState<string[] | RcFile[] | Blob[] | []>([])
    const [prevewUrl, setPrevewUrl] = React.useState<string[]>([])
    const [previewLayout, setPreviewLayout] = React.useState<React.ReactNode>()
    const [content, setContent] = React.useState<string | null>('')
    const [cursor, setCursor] = React.useState<number | null>(null)
    const [emoji, setEmoji] = React.useState('')
    const [felling, setFelling] = React.useState('')
    const [loading, setLoading] = React.useState<boolean>(false)
    const [messageApi, contextHolder] = message.useMessage();

    const setdefaultValue = () => {
        setLoading(false)
        setColor('#000')
        setFiles([])
        setPrevewUrl([])
        setContent('')
        setFelling('')
    }
    const handChangeFiles = (info: RcCustomRequestOptions) => {
        //@ts-ignore
        const objectUrl = URL.createObjectURL(info.file)
        setPrevewUrl(preve => preve.concat(objectUrl))
        //@ts-ignore
        setFiles(preve => preve.concat(info.file))
    }
    const [createPost] = useMutation(CREATE_Post, {
        onCompleted: () => {
            setdefaultValue();
            messageApi.open({
                type: 'success',
                content: 'Your post has upload successfully ! You and your friend can view the post after checking content campaign by Admintor',
                duration: 10,
            });
        },
        onError: (error) => {
            setLoading(false)
            return error
        }
    });
    React.useEffect(() => {
        switch (prevewUrl.length) {
            case 0:
                setPreviewLayout(<EmtyLayout />)
                break;
            case 1:
                setPreviewLayout(<SigleLayout urls={prevewUrl} />)
                break;
            case 2:
                setPreviewLayout(<DoubleLayout urls={prevewUrl} />)
                break;
            case 3:
                setPreviewLayout(<TripleLayout urls={prevewUrl} />)
                break;
            case 4:
                setPreviewLayout(<QuatarLayout urls={prevewUrl} />)
                break;
            default:
                setPreviewLayout(<MultipleLayout urls={prevewUrl} />)
                break;
        }
    }, [prevewUrl])

    React.useEffect(() => {
        if (cursor) {
            const text = content?.slice(0, cursor) + emoji + content?.slice(cursor)
            setContent(text)
        } else {
            setContent(preve => preve + emoji)
        }
    }, [emoji])

    const handleDestroyImg = () => {
        setPreview(false)
        if (prevewUrl.length !== 0) {
            prevewUrl.map(url => URL.revokeObjectURL(url))
            setPrevewUrl([])
            setFiles([])
        }
    }
    const onEmojiClick = (emoji: EmojiClickData) => setEmoji(emoji.emoji)
    const handleMenuClick: MenuProps['onClick'] = (e) => setFelling(e.key)

    const menuProps = {
        items,
        onClick: handleMenuClick,
    }

    const handleSubmit = async () => {
        setLoading(true)
        if (files.length !== 0) {
            const images = { myfiles: files }
            const response = await Cloud.multil(images)
            const data = {
                felling: felling,
                content: content,
                color: color,
                media: response?.data
            }
            createPost({
                variables: { ...data }
            })
        } else {
            const data = {
                felling: felling,
                content: content,
                color: color,
            }
            createPost({
                variables: { ...data }
            })
        }
    }
    return (
        <Modal
            title={<StyledTitle>Create post</StyledTitle>}
            open={open}
            onCancel={handleCancel}
            mask={true}
            bodyStyle={{
                borderTop: '1px solid #ccc'
            }}
            okText='Post'
            footer={false}
        >
            {contextHolder}
            <HeadBox>
                <Col><StyledAvatar size={46} src={user?.avatar?.src} /></Col>
                <Col>
                    <UserName>{user?.name}</UserName>
                    <p>public <FcGlobe /></p>
                </Col>
                <Felling>{felling}</Felling>
            </HeadBox>
            <BodyBox>
                <StyledInput
                    customColor={color}
                    placeholder={`What's on your mind, ${user?.name} ?`}
                    autoSize={{ minRows: 1, maxRows: 8 }}
                    bordered={false}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => { setContent(e.target.value); setCursor(null) }}
                    onClick={(e: FixTypeLater) => setCursor(e.target.selectionStart)}
                    value={content}
                />
                <PreviewSpace $actived={preview}>
                    <StyledUpload name='file' multiple={false} customRequest={(info: RcCustomRequestOptions) => handChangeFiles(info)} showUploadList={false}>
                        <Button icon={<FcImageFile />}>Add Photos/Videos</Button>
                    </StyledUpload>
                    <AiOutlineCloseCircle size={24} style={{ color: '#ccc', position: 'absolute', top: '8px', right: '12px', boxShadow: '0 0 4px #ccc;', zIndex: '45' }} onClick={() => handleDestroyImg()} />
                    {previewLayout}
                </PreviewSpace>
            </BodyBox>
            <FooterBox>
                <ColorPicker value={color} onChange={(hex: Color) => setColor(hex.toHexString())}>
                    <ColorSelectIcon src={ColorSelect} alt="color-select" />
                </ColorPicker>

                <Popover content={
                    <EmojiPicker onEmojiClick={onEmojiClick} emojiStyle={EMOJITYPE} height={350} />
                } title="Emoji picker" trigger="click">
                    <TbMoodHappy color='#ccc' size={32} />
                </Popover>
            </FooterBox>
            <ExtendBox>
                <Col span={12}>Add to your post</Col>
                <ExtendFeature span={12}>
                    <HiMiniPhoto color='#45bd62' size={24} onClick={() => setPreview(true)} />
                    <BsFillCameraVideoFill color='#f5533d' size={24} onClick={() => setPreview(true)} />
                    <Dropdown menu={menuProps} placement="top">
                        <BsFillEmojiWinkFill color='#f7b928' size={24} />
                    </Dropdown>
                    <BsFillTagsFill color='#1877f2' size={24} />
                    <FaLocationDot color='#2abba7' size={24} />
                </ExtendFeature>
            </ExtendBox>
            <Button
                type="primary"
                loading={loading}
                disabled={!content}
                onClick={() => handleSubmit()}
                block >
                Post
            </Button>
        </Modal>
    )
}

const StyledTitle = styled.div`
    text-align: center;
    font-size: 22px;
    color: #4e5d78;
`
const HeadBox = styled(Row)`
    margin: 12px 0;
    color: #4e5d78;
`
const UserName = styled.div`
    font-size: 16px;
    font-weight: 600;
`
const StyledAvatar = styled(Avatar)`
    border: solid 1px #ccc;
    margin-right: 12px;
`
const Felling = styled(Col)`
    font-size: 16px;
    margin-left: 12px;
`
const BodyBox = styled.div`
    min-height: 46px;
`
const FooterBox = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
`
const ColorSelectIcon = styled.img`
    width: 8%;
`
const ExtendBox = styled(Row)`
    margin: 12px 0;
    padding: 12px;
    border: solid 1px #ccc;
    border-radius: 8px;
`
const ExtendFeature = styled(Col)`
    display: flex;
    justify-content: space-between;
    align-items: center;
`
const PreviewSpace = styled.div`
    margin: 12px 0;
    min-height: 56px;
    border: solid 1px #ccc;
    border-radius: 8px;
    position: relative;
    padding: 2px;
    ${(props) => {
        //@ts-ignore
        if (props.$actived) return css`
        display: block;
        `
        else return css`
        display: none;
        `
    }}
`
const StyledUpload = styled(Upload)`
    position: absolute;
    top: 8px;
    left: 12px;
    box-shadow: 0 0 8px #ccc;
    z-index: 45;
`
const StyledInput = styled(TextArea)`
    padding: 0;
    font-size: 16px;
    color: ${(props) => props.customColor};
    text-decoration: none;
`
export default PostCreater
