import { useSelector } from "react-redux";
import type { FixTypeLater } from "react-redux";
import { Col, Input, Avatar, Dropdown } from "antd";
import styled from "styled-components";
import type { MenuProps } from 'antd';
import FriendCard from "../items/friend_card";
import {
    EllipsisOutlined
} from '@ant-design/icons'
import type { User } from '../../types/user'

const { Search } = Input
const items: MenuProps['items'] = [
    {
        label: 'All friends',
        key: '0',
    },
    {
        type: 'divider',
    },
    {
        label: 'Active friends',
        key: '1',
    },
];


const FriendList = () => {
    const { user, friends } = useSelector((state: FixTypeLater) => state.user)
    const handleMenuClick: MenuProps['onClick'] = (e) => {
        console.log('click', e);
    };
    const onSearch = (value: string) => console.log(value);
    const menuProps = {
        items,
        onClick: handleMenuClick,
    }
    return (
        <FriendsCol>
            <Search placeholder="Search Friends!" onSearch={onSearch} style={{ width: '100%' }} />
            <RecommentFriends>
                <RecommentAvatar size={46} src={user?.avatar?.src} />
                <RecommentAvatar size={46} src="https://plus.unsplash.com/premium_photo-1677545182425-4fb12bdb9faf?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8N3x8Y2F0JTIwZmFjZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60" />
                <RecommentAvatar size={46} src="https://images.unsplash.com/photo-1571752726703-5e7d1f6a986d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8ZmlzaHxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=400&q=60" />
                <RecommentAvatar size={46} src="https://images.unsplash.com/photo-1536862228204-9739114939ef?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTJ8fGRvZyUyMGZhY2V8ZW58MHx8MHx8fDA%3D&auto=format&fit=crop&w=400&q=60" />
            </RecommentFriends>
            <HeadList>
                <span>Friends</span>
                <Dropdown menu={menuProps} trigger={['click']} >
                    <EllipsisOutlined />
                </Dropdown>
            </HeadList>
            <div>
                {friends.map((elemet: User) => <FriendCard data={elemet} />)}
            </div>
        </FriendsCol>
    )
}

const FriendsCol = styled(Col)`
    width: 20.6%;
    background-color: #fff;
    padding: 12px 1.6%;
    position: fixed;
    top: 56px;
    right: 0;
    bottom: 0;
    overflow-y: auto;
    &::-webkit-scrollbar {
        width: 0;
    }
`
const RecommentFriends = styled.div`
    margin: 18px 0;
    font-weight: 500;
    display: flex;
    justify-content: space-between;
`
const RecommentAvatar = styled(Avatar)`
    border: solid 2px #377dff;
`
const HeadList = styled.div`
    display: flex;
    justify-content: space-between;
    font-size: 16px;
    font-weight: 500;
`

export default FriendList
