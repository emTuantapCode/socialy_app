import * as React from 'react';
import { Row } from "antd";
import styled from "styled-components";
import { LayoutProps } from '../../types/post';

export const EmtyLayout = () => {
    return <></>
}

export const SigleLayout: React.FC<LayoutProps> = ({ urls }) => {
    return (
        <RoundedContainer>
            <SigleImg src={urls[0]} alt="image_1" />
        </RoundedContainer>
    )
}

export const DoubleLayout: React.FC<LayoutProps> = ({ urls }) => {
    return (
        <RoundedContainer>
            <DoubleImg src={urls[0]} alt="image_1" />
            <DoubleImg src={urls[1]} alt="image_2" />
        </RoundedContainer>
    )
}

export const TripleLayout: React.FC<LayoutProps> = ({ urls }) => {
    return (
        <RoundedContainer>
            <DoubleImg src={urls[0]} alt="image_1" />
            <DoubleImg src={urls[1]} alt="image_2" />
            <SigleImg src={urls[2]} alt="image_3" />
        </RoundedContainer>
    )
}

export const QuatarLayout: React.FC<LayoutProps> = ({ urls }) => {
    return (
        <RoundedContainer>
            <DoubleImg src={urls[0]} alt="image_1" />
            <DoubleImg src={urls[1]} alt="image_2" />
            <DoubleImg src={urls[2]} alt="image_3" />
            <DoubleImg src={urls[3]} alt="image_4" />
        </RoundedContainer>
    )
}

export const MultipleLayout: React.FC<LayoutProps> = ({ urls }) => {
    return (
        <RoundedContainer>
            <DoubleImg src={urls[0]} alt="image_1" />
            <DoubleImg src={urls[1]} alt="image_2" />
            <DoubleImg src={urls[2]} alt="image_3" />
            <DoubleImg src={urls[3]} alt="image_3" />
            <MoreImg>+{urls.length - 4}</MoreImg>
        </RoundedContainer>
    )
}

const RoundedContainer = styled(Row)`
    border-radius: 8px;
    overflow: hidden;
    display: flex;
    align-items: stretch;
    position: relative;
`
const DoubleImg = styled.img`
    object-fit: cover;
    width: 50%;
    height: full;
`
const SigleImg = styled.img`
    object-fit: cover;
    width: 100%;
`
const MoreImg = styled.div`
    position: absolute;
    bottom: 20%;
    right: 22%;
    font-size: 40px;
    font-weight: 700;
    color: #fff;
    text-shadow: 2px 2px 8px #333;
`
