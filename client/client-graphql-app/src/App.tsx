import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AuthPage from "./pages/auth";
import LoginSuccess from './pages/login_success'
import PrivateWrapper from './constants/app/Private_wrapper'
import NewFeeds from "./pages/feeds";
import Community from "./pages/community";
import Messages from './pages/message'
import Notification from "./pages/notification";
import Blog from "./pages/blogs";
import Profile from "./pages/profile";
import Setting from "./pages/settings";
import Admin from "./pages/admin";
import Notfound from "./pages/notfound";
import path from "./constants/app/path";

function App() {
  return (
    <Router>
      <Routes >
        <Route path={path.auth} element={<AuthPage />} />
        <Route path={path.login_success} element={<LoginSuccess />} />
        <Route element={<PrivateWrapper />}>
          <Route path={path.newfeeds} element={<NewFeeds />} />
          <Route path={path.community} element={<Community />} />
          <Route path={path.messages} element={<Messages />} />
          <Route path={path.notification} element={<Notification />} />
          <Route path={path.blog} element={<Blog />} />
          <Route path={path.profile} element={<Profile />} />
          <Route path={path.setting} element={<Setting />} />
          <Route path={path.admin} element={<Admin />} />
        </Route>
        <Route path='*' element={<Notfound />} />
      </Routes>
    </Router>
  )
}

export default App
