import actionType from "../actions/action_types"
import { Action } from '../../types/redux'

const initState = {
    user: {},
    friends: []
}

const userReducer = (state = initState, action: Action) => {
    switch (action.type) {
        case actionType.GET_USER_INFO:
            return {
                ...state,
                user: action.data
            };
        case actionType.GET_FRIEND_INFO:
            return {
                ...state,
                friends: action.data
            };
        case actionType.GET_CURRENT_PROFILE:
            return {
                ...state,
                _id: action.data
            }
        default:
            return state
    }
}
export default userReducer