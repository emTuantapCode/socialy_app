import actionType from './action_types';
import actionTypes from './action_types'
import { User } from '../../types/user'

export const getUserInfo = (user: User) => {
    return {
        type: actionTypes.GET_USER_INFO,
        data: user,
    };
};

export const getFriendInfo = (friends: User[]) => {
    return {
        type: actionType.GET_FRIEND_INFO,
        data: friends
    }
}

export const getCurrentProfile = (_id: string) => {
    return {
        type: actionType.GET_CURRENT_PROFILE,
        data: _id
    }
}