const actionType = {
    GET_USER_INFO: 'GET_USER_INFO',
    GET_FRIEND_INFO: 'GET_FRIEND_INFO',
    GET_CURRENT_PROFILE: "GET_CURRENT_PROFILE"
}

export default actionType