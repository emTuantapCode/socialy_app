const formatTimeAgo = (dateString: string | undefined) => {
    if (dateString === undefined) return ''
    const currentDate: Date | number = new Date();
    const targetDate: Date | number = new Date(+dateString);
    //@ts-ignore
    const timeDifference = currentDate - targetDate;

    if (timeDifference < 60000) {
        return 'Just now';
    }
    const seconds = Math.floor(timeDifference / 1000);
    if (seconds < 3600) {
        const minutes = Math.floor(seconds / 60);
        return `${minutes}m`;
    }
    const hours = Math.floor(seconds / 3600);
    if (hours < 24) {
        return `${hours}h`;
    }

    const days = Math.floor(hours / 24);
    return `${days}d`;
}

export default formatTimeAgo
