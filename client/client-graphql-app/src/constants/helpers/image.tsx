export const sizedAvatar = (path: string) => {
    const regex = /\/v[\d]+/;
    const size = "/w_500,h_500";
    return path.replace(regex, size)
}