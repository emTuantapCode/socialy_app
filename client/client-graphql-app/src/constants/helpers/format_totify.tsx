import type { Notify, NotifyFormated } from "../../types/notification"
import { AiOutlineComment, AiOutlineUserAdd } from 'react-icons/ai'
import { FaCommentDots } from 'react-icons/fa'
import { BsChatSquareHeartFill } from 'react-icons/bs'
import { MdOutlineNewspaper } from 'react-icons/md'


export const notifyFormater = (notifies: Notify[]): NotifyFormated[] => Array.from(notifies, (notify: Notify) => {
    const notifyFormated: NotifyFormated = { ...notify, typeIcon: undefined, content: undefined }
    switch (notify?.type) {
        case 'Comment':
            notifyFormated.typeIcon = <AiOutlineComment color="#38cb89" size={22} />
            notifyFormated.content = `${notify?.source?.name} Commented on your post`
            notifyFormated.status = (notifyFormated.status === 'unread') ? true : false
            break;
        case 'Message':
            notifyFormated.typeIcon = <FaCommentDots color="#377dff" />
            notifyFormated.content = `${notify?.source?.name} Sent you messages`
            notifyFormated.status = (notifyFormated.status === 'unread') ? true : false
            break;
        case 'Add Friend':
            notifyFormated.typeIcon = <AiOutlineUserAdd color="#ffab00" size={22} />
            notifyFormated.content = `${notify?.source?.name} Sent you add friend request`
            notifyFormated.status = (notifyFormated.status === 'unread') ? true : false
            break;
        case 'Like':
            notifyFormated.typeIcon = <BsChatSquareHeartFill color="#ff5630" />
            notifyFormated.content = `${notify?.source?.name} Liked your post`
            notifyFormated.status = (notifyFormated.status === 'unread') ? true : false
            break;
        case 'Like Comment':
            notifyFormated.typeIcon = <BsChatSquareHeartFill color="#377dff" />
            notifyFormated.content = `${notify?.source?.name} Liked your comment`
            notifyFormated.status = (notifyFormated.status === 'unread') ? true : false
            break;
        case 'New Post':
            notifyFormated.typeIcon = <MdOutlineNewspaper color="#38cb89" />
            notifyFormated.content = `${notify?.source?.name} Posted some news`
            notifyFormated.status = (notifyFormated.status === 'unread') ? true : false
            break;
    }
    return notifyFormated;
})