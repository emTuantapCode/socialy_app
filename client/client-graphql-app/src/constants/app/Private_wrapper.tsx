import * as React from "react";
import { Outlet } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import type { FixTypeLater } from "react-redux";
import { Layout, Row, Col, Menu, ConfigProvider, Input } from "antd";
import type { MenuProps } from "antd"
import { MenuInfo } from "rc-menu/lib/interface";
import styled, { css } from "styled-components";
import { siderTitle, siderPath } from './system'
import AdminMenu from "./admin_menu";
import { useQuery } from '@apollo/client';
import { GET_Users } from "../../apis/user";
import {
    AppstoreOutlined,
    TeamOutlined,
    MessageOutlined,
    BellOutlined, GlobalOutlined,
    UserOutlined,
    SettingOutlined,
    DisconnectOutlined,
    SearchOutlined,
    LoadingOutlined,
    AliwangwangOutlined
} from '@ant-design/icons'
import * as action from '../../store/actions'
import appImg from '../../assets/app.png'
import { useLocation } from "react-router-dom";

const LOG_OUT = 'logout'
const { Header } = Layout
const items: MenuProps['items'] = [
    AppstoreOutlined,
    TeamOutlined,
    MessageOutlined,
    BellOutlined, GlobalOutlined,
    UserOutlined,
    SettingOutlined,
    DisconnectOutlined,
].map((icon, index) => ({
    key: siderPath[index],
    icon: React.createElement(icon),
    label: siderTitle[index],
}));


const PrivateWrapper = () => {
    const location = useLocation()
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const { user } = useSelector((state: FixTypeLater) => state.user)
    const [searching, setSearching] = React.useState(false)
    const [selectedItems, setSelectedItems] = React.useState([location.pathname])
    React.useEffect(() => {
        const isAuth = !!(user?.role_code && localStorage.getItem('token'))
        if (!isAuth) {
            navigate('/')
        }
    }, [navigate, user])
    const handleChangePage = (info: MenuInfo) => {
        if (info.key === LOG_OUT) {
            localStorage.removeItem("token");
            navigate('/')
            return
        }
        navigate(info.key)
    }
    const { refetch } = useQuery(GET_Users, {
        variables: { searching: { isFriend: true } },
        onCompleted: (data) => {
            dispatch(action.getFriendInfo(data?.getUsers))
        }
    });
    React.useEffect(() => {
        refetch()
    }, [])
    React.useEffect(() => {
        setSelectedItems([location.pathname])
    }, [location.pathname])

    return (<>
        <ConfigProvider theme={{
            components: {
                Menu: {
                    activeBarBorderWidth: 0,
                    collapsedIconSize: 18,
                    itemSelectedColor: '#fff',
                    itemSelectedBg: '#4e5d78',
                    itemColor: '#4e5d78'
                },
            },
        }}>
            <Layout style={{ backgroundColor: '#fff' }}>
                <StyledHeader>
                    <Row>
                        <StyledImg span={4} ></StyledImg>
                        <SearchSpace span={14} >
                            <StyledInput
                                placeholder="Searching for users, community, blog..."
                                prefix={<SearchOutlined />}
                                onFocus={() => setSearching(true)}
                                onBlur={() => setSearching(false)} />
                            <SearchResult $display={searching}>
                                <span>
                                    <AliwangwangOutlined /> I'm watching ... <LoadingOutlined />
                                </span>
                            </SearchResult>
                        </SearchSpace>
                        <StyledInfor span={6} >
                            <div>{user?.name}</div>
                            <StyledBlockAvatar src={user?.avatar?.src} alt={user?.avatar?.filename} />
                        </StyledInfor>
                    </Row>
                </StyledHeader>
                <StyledLayout>
                    <StyledSider span={4}>
                        <AdminMenu />
                        <Menu onClick={(info) => handleChangePage(info)} mode="inline" defaultSelectedKeys={[siderPath[0]]} selectedKeys={selectedItems} items={items} />
                    </StyledSider>
                    <Col span={20} offset={4}>
                        <Outlet />
                    </Col>
                </StyledLayout>
                <StyledCopyRight>@coppy right by AnhDT - 2023</StyledCopyRight>
            </Layout>
        </ConfigProvider>
    </>)
}

const StyledHeader = styled(Header)`
    position: fixed;
    z-index: 50;
    height: 56px;
    width: 100%;
    background-color: #fff;
    padding: 0 2%;
    line-height: 56px;
    color: #4e5d78;
    font-size: 16px;
    font-weight: 600;
`
const StyledLayout = styled(Row)`
    margin-top: 56px;
    background-color: #fff;
    color: #4e5d78;
`
const StyledImg = styled(Col)`
    background: url(${appImg});
    background-size: contain;
    background-repeat: no-repeat;
`
const StyledInfor = styled(Col)`
    display: flex;
    justify-content: end;
    align-items: center;
`
const StyledBlockAvatar = styled.img`
    margin-left: 8px;
    height: 42px;
    border-radius: 8px;
`
const StyledSider = styled(Col)`
    position: fixed;
    z-index: 50;
    top: 56px;
    left: 0;
    bottom: 0;
    background-color: #fff;
    width: 90%;
    font-size: 16px;
    font-weight: 500;
`
const StyledCopyRight = styled.span`
    position: fixed;
    z-index: 55;
    bottom: 3%;
    left: 1%;
    font-size: 10px;
    font-weight: 600;
`
const StyledInput = styled(Input)`
    width: 68%;
`
const SearchSpace = styled(Col)`
    position: relative;
`
const SearchResult = styled.div`
    ${(props) => {
        //@ts-ignore
        if (props.$display) return css`display: block;`
        else return css`display: none;`
    }}
    position: absolute;
    background-color: #fff;
    width: 68%;
    box-shadow: 6px 8px 6px -6px #ccc, -6px 8px 6px -6px #ccc;
    border-radius: 4px;
    top: 50px;
    text-align: center;
    font-size: 16px;
`

export default PrivateWrapper
