import { siderPath } from "./system"
const path = {
    auth: '/',
    login_success: '/login-success',
    pages: '/pages',
    newfeeds: siderPath[0],
    community: siderPath[1],
    messages: siderPath[2],
    notification: siderPath[3],
    blog: siderPath[4],
    profile: siderPath[5],
    setting: siderPath[6],
    admin: '/admin'
}

export default path
