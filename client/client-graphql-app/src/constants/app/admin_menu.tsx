import * as React from "react";
import { useSelector } from "react-redux"
import type { FixTypeLater } from "react-redux";
import { useNavigate } from "react-router-dom";
import { Menu } from "antd";
import { MenuInfo } from "rc-menu/lib/interface";
import type { MenuProps } from "antd"
import {
    FireOutlined
} from '@ant-design/icons'

const items: MenuProps['items'] = [
    FireOutlined
].map((icon) => ({
    key: '/admin',
    icon: React.createElement(icon),
    label: 'Admin',
}));

const AdminMenu = () => {
    const navigate = useNavigate()
    const { user } = useSelector((state: FixTypeLater) => state.user)
    const secretRole = import.meta.env.VITE_SECRET_ROLE
    const isAdmin = user?.role_code === secretRole
    const handleChangePage = (info: MenuInfo) => {
        navigate(info.key)
    }
    return (
        <>
            {isAdmin && <Menu onClick={(info) => handleChangePage(info)} mode="inline" items={items} />}
        </>
    )
}

export default AdminMenu
