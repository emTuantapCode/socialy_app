import axios, { InternalAxiosRequestConfig } from 'axios'

const axiosInstance = axios.create({
    baseURL: import.meta.env.VITE_SERVER_URI,
    headers: {
        'Content-Type': 'application/json'
    }
})

axiosInstance.interceptors.request.use((config: InternalAxiosRequestConfig) => {
    //@ts-ignore
    const token: string = window.localStorage.getItem('token') || null
    //@ts-ignore
    if (token) config.headers = {
        authorization: token
    }
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
axiosInstance.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    if (response && response.data) return response.data;
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    Promise.reject(error);
    return error?.response?.data;
});

export default axiosInstance