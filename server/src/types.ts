export type systemVariant = any;
export type clientConfigKey = String | undefined;
export interface File {
    fieldname?: String,
    originalname?: String,
    encoding?: String,
    mimetype?: String,
    path: String,
    size?: Number,
    filename: String
}
export interface FormatedFile {
    filename: String,
    mediasrc: String
}