import cloudinary from 'cloudinary'
import { CloudinaryStorage } from 'multer-storage-cloudinary'
import multer from 'multer'

cloudinary.v2.config({
    cloud_name: process.env.CLOUD_NAME || 'dciozzifg',
    api_key: process.env.CLOUD_KEY || '956686345645342',
    api_secret: process.env.CLOUD_SECRET || 'AxIvq1VWdk0jLmrZgQ7qz2tGYrs',
})

const storage = new CloudinaryStorage({
    cloudinary: cloudinary.v2,
    allowedFormats: ['jpg', 'png'],
    params: {
        //@ts-ignore
        folder: process.env.CLOUD_FOLDER || 'graphql_cloud',
    }
});

export const uploadCloud = multer({
    storage,
});

export const destroyMedia = async (fileName: any) => {
    cloudinary.v2.config({
        cloud_name: process.env.CLOUD_NAME,
        api_key: process.env.CLOUD_KEY,
        api_secret: process.env.CLOUD_SECRET,
    })
    try {
        return await cloudinary.v2.uploader.destroy(fileName)
    } catch (error) {
        console.log(`Cloud error: ${error}`)
    }
}
