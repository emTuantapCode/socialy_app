import passport from "passport";
import User from '../modules/users'
import { Strategy as GoogleStrategy } from 'passport-google-oauth20';
import { systemVariant, clientConfigKey } from '../types'

const startGoogleStrategy = () => {
    passport.use(new GoogleStrategy({
        //@ts-ignore
        clientID: process.env.GOOGLE_ID,
        //@ts-ignore
        clientSecret: process.env.GOOGLE_SECRET,
        callbackURL: "/api/google/callback",
        scope: ['profile', 'email'],
    },
        //@ts-ignore
        async (accessToken: clientConfigKey, refreshToken: clientConfigKey, profile: systemVariant, cb: systemVariant) => {
            try {
                // Do somethings with google token
                if (profile?.id) {
                    const user = await User.findOne({ googleID: profile.id })
                    if (user) {
                        const response = {
                            isNew: false,
                            user: user
                        }
                        return cb(null, response)
                    }
                    const newUser = await User.create({
                        googleID: profile.id,
                        email: profile._json.email,
                        name: profile._json.name,
                        typeLogin: profile.provider,
                        role_code: (profile._json.email != process.env.EMAIL_ADMIN)
                            ? process.env.ROLE_DEFAULT : process.env.ROLE_ADMIN,
                        avatar: {
                            filename: 'googleSRC',
                            src: profile.photos[0]?.value
                        },
                    })
                    const response = {
                        isNew: true,
                        user: newUser
                    }
                    return cb(null, response)
                }
                return cb(null, profile)
            } catch (error) {
                console.log(error)
                return cb(error, null)
            }
        }
    ))
}

export default startGoogleStrategy
