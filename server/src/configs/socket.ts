import { Server } from 'socket.io';
import { systemVariant } from 'types';

const configSocketIO = (httpServer: systemVariant) => {
    const io = new Server(httpServer, {
        cors: {
            origin: "*"
        },
    });
    return io;
}

export default configSocketIO;
