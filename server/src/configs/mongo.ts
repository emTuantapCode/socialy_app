import mongoose from "mongoose";

const dbConnect = async () => {
    try {
        await mongoose.connect(process.env.MONGODB_URI || 'mongodb://0.0.0.0:27017/')
        console.log('🎃 DB connected successfully!');
    } catch (error) {
        console.log(`XD DB connection failed!`)
        throw new Error(error)
    }
}

export default dbConnect;
