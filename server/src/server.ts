import { ApolloServer } from '@apollo/server';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import express from 'express';
import http from 'http';
import cors from 'cors';
import bodyParser from 'body-parser';
import { typeDefs, resolvers } from './schemas'
import dbConnect from './configs/mongo'
import dotenv from 'dotenv'
import initialAPIs from './apis'
import initialSocketIO from './socket_io'
import startGoogleStrategy from './configs/passport'

interface MyContext {
    token?: string;
}
dotenv.config()
// Required logic for integrating with Express
const app = express();
// Our httpServer handles incoming requests to our Express app.
// Below, we tell Apollo Server to "drain" this httpServer,
// enabling our servers to shut down gracefully.
const httpServer = http.createServer(app);
// Same ApolloServer initialization as before, plus the drain plugin
// for our httpServer.
// @ts-ignore
const server = new ApolloServer<MyContext>({
    typeDefs,
    resolvers,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })]
});
// Ensure we wait for our server to start
(async () => {
    startGoogleStrategy()
    initialSocketIO(httpServer)
    // @ts-ignore
    await server.start()
    // Set up our Express middleware to handle CORS, body parsing,
    // and our expressMiddleware function.
    app.use(
        cors<cors.CorsRequest>({
            origin: process.env.CLIENT_URI
        }),
        bodyParser.json(),
    )
    initialAPIs(app, server)
    // Modified server startup
    await new Promise<void>((resolve) => httpServer.listen({ port: process.env.PORT }, resolve))
    await dbConnect()
    console.log(`🚀 Server ready at http://localhost:${process.env.PORT}`)
    console.log(`🚀 cors origin at ${process.env.CLIENT_URI}`)
})()
