import configSocketIO from "../configs/socket";
import { systemVariant } from "types";

interface User {
    socketID?: String,
    _id: String,
    name: String,
    avatar: String
}

interface Info {
    user: User,
    friendList: String[],
    content?: String
}

interface p2pInfo {
    source: User,
    destination: User,
    content?: String
}

interface roomInfo {
    source: User,
    membersList: String[],
    content: String,
    roomID: String
}

var onlineUsers: User[] = []

const addUser = (user: User) => {
    onlineUsers.push(user)
}

const removeUser = (leaveUser: User) => {
    onlineUsers = onlineUsers.filter(user => user._id !== leaveUser._id)
}

const filterOnline = (people: String[]) => {
    return onlineUsers.filter(user => people.includes(user._id))
}

const initialSocketIO = (httpServer: systemVariant) => {
    const io = configSocketIO(httpServer)
    io.on('connection', (socket: systemVariant) => {
        socket.on('addUser', ({ user, friendList }: Info) => {
            user.socketID = socket.id
            addUser(user)
            const onlineFriends = filterOnline(friendList)
            onlineFriends.length !== 0 && onlineFriends.forEach(friend => {
                //@ts-ignore
                io.to(friend?.socketID).emit('getOnlineUser', {
                    data: onlineFriends
                })
            })
        })
        socket.on('approvedPost', (info: Info) => {
            //@ts-ignore
            io.to(info?.user?.socketID).emit('getPostStatus', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was approved by admin`
            })
            const onlineFriends = filterOnline(info?.friendList)
            onlineFriends.length !== 0 && onlineFriends.forEach(friend => {
                //@ts-ignore
                io.to(friend?.socketID).emit('getPost', {
                    mess: `${info.user.name} has a new post ${info.content ? ': ' + info.content : ''}`
                })
            })
        })
        socket.on('deniedPost', (info: Info) => {
            //@ts-ignore
            io.to(info?.user?.socketID).emit('getPostStatus', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was denied by admin`
            })
        })
        socket.on('addComment', (info: p2pInfo) => {
            //@ts-ignore
            io.to(info?.destination?.socketID).emit('getComment', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was denied by admin`
            })
        })
        socket.on('addComment', (info: p2pInfo) => {
            //@ts-ignore
            io.to(info?.destination?.socketID).emit('getComment', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was denied by admin`
            })
        })
        socket.on('addMessage', (info: roomInfo) => {
            const onlineMembers = filterOnline(info?.membersList)
            onlineMembers.length !== 0 && onlineMembers.forEach(member => {
                //@ts-ignore
                io.to(member?.socketID).emit('getMessage', {
                    room: info.roomID,
                    mess: `${info.source?.name} sent you a message`,
                    content: info.content
                })
            })
        })
        socket.on('disconnection', ({ user, friendList }: Info) => {
            removeUser(user)
            const onlineFriends = onlineUsers.filter(user => friendList.includes(user._id))
            onlineFriends.length !== 0 && onlineFriends.forEach(friend => {
                //@ts-ignore
                io.to(friend?.socketID).emit('getOnlineUser', {
                    data: onlineFriends
                })
            })
        })
    })
}

export default initialSocketIO;
