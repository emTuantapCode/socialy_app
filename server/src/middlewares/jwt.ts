import jwt from 'jsonwebtoken'

export const generateCommonToken = (_id: String, role_code: String, expiresIn: String) => {
    // @ts-ignore process can not match
    return jwt.sign({ _id, role_code }, process.env.JWT_SECRET, { expiresIn: expiresIn })
}
