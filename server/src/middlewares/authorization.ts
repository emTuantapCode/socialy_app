import { systemVariant } from '../types'
import jwt from 'jsonwebtoken'

export const requireToken = (req: systemVariant, res: systemVariant, next: systemVariant) => {
    let token = req.headers.authorization
    if (!token) {
        return res.status(401).json('Require authorization token')
    } else {
        // @ts-ignore process can not match
        const err = jwt.verify(token, process.env.JWT_SECRET, (err, decode) => {
            req.headers.identify = decode
            return err
        })
        if (err) return res.status(401).json('Unauthorized token')
        next()
    }
}