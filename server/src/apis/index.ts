import { expressMiddleware } from '@apollo/server/express4';
import { graphqlUploadExpress } from 'graphql-upload-ts/dist'
import passport from 'passport';
import { generateCommonToken } from '../middlewares/jwt'
import { requireToken } from '../middlewares/authorization'
import { systemVariant } from '../types'
import { uploadCloud } from '../configs/cloud';
import type { File, FormatedFile } from '../types'

const initialAPIs = (app: systemVariant, server: systemVariant) => {
    app.get('/api/google/service', passport.authenticate('google', { scope: ['profile', 'email'] }))
    app.get('/api/google/callback', (req: systemVariant, res: systemVariant, next: systemVariant) => {
        passport.authenticate('google', (err: systemVariant, response: systemVariant) => {
            if (err) return res.status(502).json('Error :' + err)
            req.user = response.user
            req.isNew = response.isNew
            next()
        })(req, res, next)
    }, (req: systemVariant, res: systemVariant) => {
        // @ts-ignore process can not match client uri
        const token = generateCommonToken(req.user?._id, req.user?.role_code, '1d')
        res.redirect(`${process.env.CLIENT_URI}/login-success?is-new=${req.isNew}&token=${token}`)
    })
    app.post('/api/cloudinary/single-upload', requireToken, uploadCloud.single('myfile'), (req: systemVariant, res: systemVariant) => {
        try {
            const response = {
                filename: req.file?.filename,
                src: req.file?.path
            }
            return res.status(200).json(response)
        } catch (error) {
            throw new Error(error)
        }
    })
    app.post('/api/cloudinary/multil-upload', requireToken, uploadCloud.any(), (req: systemVariant, res: systemVariant) => {
        try {
            const response: FormatedFile[] = []
            const files = req.files
            files.map((file: File) => {
                const element: FormatedFile = {
                    filename: file.filename,
                    mediasrc: file.path
                }
                response.push(element)
            })
            return res.status(200).json(response)
        } catch (error) {
            throw new Error(error)
        }
    })
    app.use(
        '/api/graphql',
        requireToken,
        graphqlUploadExpress(),
        // expressMiddleware accepts the same arguments:
        // an Apollo Server instance and optional configuration options
        expressMiddleware(server, {
            context: async ({ req }) => ({ user: req.headers.identify }),
        }),
    );
    app.use((res: systemVariant) => res.status(404).json('Can not find page'))
}

export default initialAPIs
