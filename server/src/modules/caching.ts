import mongoose, { Schema } from "mongoose"; // Erase if already required

// Declare the Schema of the Mongo model
var cachingSchema = new Schema({
    type: {
        type: String,
        required: true,
    },
    source: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    },
    destination: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
    },
    content: {
        type: String,
        required: true,
    }
}, { timestamps: true });
//Export the model
export default mongoose.model('Caching', cachingSchema)
