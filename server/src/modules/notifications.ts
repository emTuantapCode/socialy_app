import mongoose, { Schema } from "mongoose"; // Erase if already required

// Declare the Schema of the Mongo model
var notifySchema = new Schema({
    type: {
        type: String,
        required: true,
        enum: ['Comment', 'Message', 'Add Friend', 'Like', 'New Post'],
    },
    source: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    },
    destination: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
    },
    status: {
        type: String,
        required: true,
        enum: ['read', 'unread'],
        default: 'unread'
    },
    uri: {
        type: String,
        required: true
    }
}, { timestamps: true });
//Export the model
export default mongoose.model('Notify', notifySchema)
