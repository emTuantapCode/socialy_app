import mongoose, { Schema } from "mongoose"; // Erase if already required

// Declare the Schema of the Mongo model
var postSchema = new Schema({
    color: {
        type: String,
    },
    content: {
        type: String,
        required: true,
    },
    status: {
        type: String,
        enum: ['Pending', 'Rejected', 'Aproved'],
        default: 'Pending'
    },
    felling: {
        type: String
    },
    media: [{
        filename: String,
        mediasrc: String
    }],
    author: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    },
    favorites: [{
        type: mongoose.Types.ObjectId,
        ref: 'User',
        default: []
    }]
}, { timestamps: true });
//Export the model
export default mongoose.model('Post', postSchema)
