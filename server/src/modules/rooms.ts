import mongoose, { Schema } from "mongoose"; // Erase if already required

// Declare the Schema of the Mongo model
var roomSchema = new Schema({
    members: [{
        type: mongoose.Types.ObjectId,
        ref: 'User',
    }]
}, { timestamps: true });
//Export the model
export default mongoose.model('Room', roomSchema)
