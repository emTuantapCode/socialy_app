import mongoose, { Schema } from "mongoose"; // Erase if already required

// Declare the Schema of the Mongo model
var userSchema = new Schema({
    googleID: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
    },
    typeLogin: {
        type: String,
    },
    status: {
        type: String,
        enum: ['Single', 'Married', 'Loving', 'Secret'],
    },
    gender: {
        type: String,
        enum: ['Male', 'Female', 'Others', 'Secret']
    },
    name: {
        type: String,
        required: true,
    },
    age: {
        type: Number,
    },
    bio: {
        type: String,
    },
    phone: {
        type: String,
    },
    address: {
        type: String,
    },
    avatar: {
        filename: String,
        src: String
    },
    role_code: {
        type: String,
        required: true
    },
    friends: [{
        type: mongoose.Types.ObjectId,
        ref: 'User',
        default: []
    }]
});
//Export the model
export default mongoose.model('User', userSchema)
