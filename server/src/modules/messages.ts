import mongoose, { Schema } from "mongoose"; // Erase if already required

// Declare the Schema of the Mongo model
var mesageSchema = new Schema({
    roomID: {
        type: mongoose.Types.ObjectId,
        ref: 'Room',
    },
    sender: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
    },
    content: {
        type: String,
        required: true
    }
}, { timestamps: true });
//Export the model
export default mongoose.model('Mesage', mesageSchema)
