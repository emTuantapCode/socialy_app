import mongoose, { Schema } from "mongoose"; // Erase if already required

// Declare the Schema of the Mongo model
var commentSchema = new Schema({
    author: {
        type: mongoose.Types.ObjectId,
        ref: 'User',
        required: true
    },
    post: {
        type: mongoose.Types.ObjectId,
        ref: 'Post',
        required: true
    },
    content: {
        type: String,
        required: true,
    },
    favorites: [{
        type: mongoose.Types.ObjectId,
        ref: 'User',
        default: []
    }]
}, { timestamps: true });
//Export the model
export default mongoose.model('Comment', commentSchema)
