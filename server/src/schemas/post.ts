import Postdb from "../modules/posts";
import Userdb from '../modules/users'
import Commentdb from '../modules/comments'
import { systemVariant } from '../types'
import { destroyMedia } from '../configs/cloud'

interface createPost {
  color?: String,
  content: String,
  felling?: String,
  author?: String,
  media?: [Object]
}

interface updatePost {
  _id: String,
  color?: String,
  content?: String,
  felling?: String,
  media?: [Object],
  prevMedia?: [Object] | null
}

interface queries {
  status: String,
  createdAt?: Object
}

const ADMIN = 'f0'

// Resolvers define how to fetch the types defined in your schema
// This resolver retrieves books from the "books" array above.
export const postResolvers = {
  // This maps the `Upload` scalar to the implementation provided
  // by the `graphql-upload` package
  Query: {
    getPost: async (_: any, args: systemVariant, context: systemVariant) => {
      try {
        const conditions = args.searching
        const { friends }: any = await Userdb.findById(context.user._id)
        const posts = Postdb.find({
          author: {
            $in: friends
          }
        })
        posts.sort({ 'createdAt': -1 })
        const page = conditions?.page || 1
        const limit = conditions?.limit || process.env.LIMIT_DEFAULT
        const skip = (page - 1) * limit
        posts.skip(skip).limit(limit)
        return await posts.exec()
      } catch (error) {
        console.log('getPost ' + error)
        throw new Error(error)
      }
    },
    getPostAdmin: async (_: any, args: systemVariant, context: systemVariant) => {
      try {
        if (context.user.role_code !== ADMIN) throw new Error('require admin role')
        const conditions = args.searching
        const queries: queries = { status: conditions.status }
        if (conditions.startDate && conditions.endDate) {
          queries.createdAt = { '$gt': conditions.startDate, '$lt': conditions.endDate }
        }
        const response = Postdb.find(queries)
        const page = conditions?.page || 1
        const limit = conditions?.limit || process.env.LIMIT_DEFAULT
        const skip = (page - 1) * limit
        response.skip(skip).limit(limit)
        return await response.exec()
      } catch (error) {
        console.log('getPostAdmin ' + error)
        throw new Error(error)
      }
    },
  },
  Post: {
    author: async (parent: systemVariant, _: systemVariant) => {
      return await Userdb.findById(parent?.author)
    },
    favoriters: async (parent: systemVariant, _: systemVariant) => {
      const response = await Userdb.find({ _id: { $in: parent?.favorites } }).select('_id name avatar')
      return response
    },
    favorited: async (parent: systemVariant, _: systemVariant, context: systemVariant) => {
      return parent?.favorites.includes(context.user._id)
    },
    favoriteNumber: async (parent: systemVariant) => {
      return parent?.favorites.length
    },
    comment: async (parent: systemVariant, _: systemVariant) => {
      return await Commentdb.find({ post: parent._id })
    },
    commentNumber: async (parent: systemVariant) => {
      return await Commentdb.countDocuments({ post: parent._id })
    },
  },
  Mutation: {
    createPost: async (_: any, args: createPost, context: systemVariant) => {
      args.author = context.user._id
      return await Postdb.create({ ...args })
    },
    updatePost: async (_: any, args: updatePost, context: systemVariant) => {
      try {
        const { _id, prevMedia, ...res } = args
        prevMedia && prevMedia.forEach(async (media) => {
          //@ts-ignore
          const response = await destroyMedia(media?.filename)
        })
        return await Postdb.findOneAndUpdate({ _id, author: context.user._id }, {
          $set: res
        }, { new: true })
      } catch (error) {
        console.log('updatePost ' + error)
        throw new Error(error)
      }
    },
    changeFavoritePost: async (_: any, args: systemVariant, context: systemVariant) => {
      try {
        const post = await Postdb.findById(args._id)
        if (!post) throw new Error('Can not find comment')
        const index = post?.favorites.indexOf(context.user._id)
        if (index !== -1) {
          post?.favorites.splice(index, 1)
        } else {
          post?.favorites.push(context.user._id)
        }
        await post.save()
        console.log(post)
        return post
      } catch (error) {
        console.log("changeFavoritePost" + error)
        throw new Error(error)
      }
    },
    deletePost: async (_: any, args: systemVariant, context: systemVariant) => {
      try {
        const post = await Postdb.findOneAndRemove({ _id: args._id, author: context.user._id })
        post?.media.forEach(async (media) => {
          //@ts-ignore
          const cloud = await destroyMedia(media?.filename)
        })
        return post ? true : false
      } catch (error) {
        console.log('deletePost ' + error)
        throw new Error(error)
      }
    },
  }
};


export const postTypeDefs = `#graphql
  # The implementation for this scalar is provided by the
  # 'GraphQLUpload' export from the 'graphql-upload-ts' package
  # in the resolvers file.

  input MediaObject {
    filename: String!
    mediasrc: String!
  }

  type Media {
    filename: String!
    mediasrc: String!
  }

  input filerPost {
    page: String
    limit: String
  }

  input filerPostAdmin {
    status: String !
    startDate: String
    endDate: String
    page: Int
    limit: Int
  }

  # This "Posts" type defines the queryable fields for every post in our data source.
  type Post {
    _id: ID
    color: String
    author : User
    content : String
    felling : String
    media: [Media]
    favoriters: [User]
    favorited: Boolean
    favoriteNumber: Int
    comment: [Comment]
    commentNumber: Int
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  type Query {
    getPost(searching: filerPost!): [Post]
    getPostAdmin(searching: filerPostAdmin!): [Post]
  }

  # The "Mutation" type is speacial: it lists all of the available features that
  # clients can execute. In this
  type Mutation {
    createPost(color: String! , content: String! , felling: String, media: [MediaObject]): Post
    updatePost(_id:ID!, color: String , content: String , felling: String, media: [MediaObject]): Post
    changeFavoritePost(_id: ID!): Post
    deletePost(_id:ID!): Post
  }
`;
