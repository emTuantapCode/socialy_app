import { systemVariant } from '../types'
import Commentdb from '../modules/comments'
import Userdb from '../modules/users'
import Postdb from '../modules/posts'

export const commentResolvers = {
    Comment: {
        post: async (parent: systemVariant, _: systemVariant) => {
            return await Postdb.findById(parent?.post)
        },
        favorite: async (parent: systemVariant, _: systemVariant, context: systemVariant) => {
            return parent?.favorites.includes(context.user._id)
        },
        favoriteNumber: async (parent: systemVariant) => {
            return parent?.favorites.length
        },
        author: async (parent: systemVariant) => {
            return await Userdb.findById(parent.author)
        }
    },
    Mutation: {
        createComment: async (_: any, args: systemVariant, context: systemVariant) => {
            return await Commentdb.create({ ...args.data, author: context.user._id })
        },
        changeFavoriteComment: async (_: any, args: systemVariant, context: systemVariant) => {
            try {
                const comment = await Commentdb.findById(args._id)
                if (!comment) throw new Error('Can not find comment')
                const index = comment?.favorites.indexOf(context.user._id)
                if (index !== -1) {
                    comment?.favorites.splice(index, 1)
                } else {
                    comment?.favorites.push(context.user._id)
                }
                await comment.save()
                return comment
            } catch (error) {
                console.log("changeFavoriteComment" + error)
                throw new Error(error)
            }
        },
        deleteComment: async (_: any, args: systemVariant, context: systemVariant) => {
            return await Commentdb.findOneAndDelete({ _id: args._id, author: context.user._id })
        }
    }
};


export const commentTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.
  
    input fieldsComment {
      post: ID!
      content: String!
    }
  
    # This "Comment" type defines the queryable fields for every post in our data source.
    type Comment {
      _id: ID
      author: User
      post: Post
      content: String
      favorite: Boolean
      favoriteNumber: Int
      createdAt: String
    }
  
    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this

  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createComment(data: fieldsComment): Comment
        changeFavoriteComment(_id: ID!): Comment
        deleteComment(_id: ID!): Comment
    }
  `;