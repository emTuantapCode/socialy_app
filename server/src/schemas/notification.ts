import Notificationdb from "../modules/notifications";
import Userdb from '../modules/users'
import { systemVariant } from '../types'

export const notifyResolvers = {
    Query: {
        getNotifications: async (_: systemVariant, _args: systemVariant, context: systemVariant) => {
            return Notificationdb.find({ destination: context.user._id })
        }
    },
    Notify: {
        source: async (parent: systemVariant, _: systemVariant) => {
            return await Userdb.findById(parent.source)
        },
        destination: async (parent: systemVariant, _: systemVariant) => {
            return await Userdb.findById(parent.destination)
        }
    },
    Mutation: {
        createNotify: async (_: any, args: systemVariant) => {
            return await Notificationdb.create({ members: args.info })
        },
        deleteNotify: async (_: any, args: systemVariant) => {
            const response = await Notificationdb.findOneAndDelete({ _id: args._id })
            await Notificationdb.deleteMany({ roomID: response?._id })
            return response
        }
    }
};


export const notifyTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.

    # This "Comment" type defines the queryable fields for every post in our data source.
    type Notify {
      _id: ID
      type: String
      source: User
      destination: User
      status: String
      uri: String
    }
  
    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    type Query {
        getNotifications: [Notify]
    }
  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createNotify(info: [ID!]): Room
        deleteNotify(_id: ID!): Room
    }
  `;