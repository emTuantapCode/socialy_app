import { systemVariant } from '../types'
import Roomdb from '../modules/rooms'
import Messagedb from '../modules/messages'
import Userdb from '../modules/users'

export const messageResolvers = {
    Message: {
        sender: async (parent: systemVariant, _: systemVariant) => {
            return await Userdb.findById(parent.sender)
        },
        roomID: async (parent: systemVariant, _: systemVariant) => {
            return await Roomdb.findById(parent.roomID)
        }
    },
    Mutation: {
        createMessage: async (_: any, args: systemVariant, context: systemVariant) => {
            return await Messagedb.create({ ...args.info, sender: context.user._id })
        }
    }
};


export const messageTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.

    input MessageInfo {
        roomID: ID!
        content: String!
    }

    # This "Comment" type defines the queryable fields for every post in our data source.
    type Message {
      _id: ID
      roomID: Room
      sender: User
      content: String
    }
  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createMessage(info: MessageInfo!): Message
    }
  `;