import { systemVariant } from '../types'
import Userdb from '../modules/users'
import Postdb from "../modules/posts";
import { destroyMedia } from '../configs/cloud'

const OUT_SRC = 'googleSRC'
const CLOUD_SUCCESS = 'ok'
const ACCEPTED = 'Pending'

export const userResolvers = {
    Query: {
        getProfile: async (_: systemVariant, args: systemVariant, context: systemVariant) => {
            try {
                context.conditions = args.searching
                return await Userdb.findById({ _id: args.searching._id || context.user._id })
            } catch (error) {
                console.log("getProfile" + error)
                throw new Error(error)
            }
        },
        getUsers: async (_: systemVariant, args: systemVariant, context: systemVariant) => {
            try {
                if (args.searching.isFriend) {
                    //@ts-ignore
                    const { friends } = await Userdb.findById(context.user._id)
                    const response = Userdb.find({
                        _id: { $in: friends }
                    })
                    const regex = new RegExp(args.searching.key, 'i')
                    args.searching?.key && response.find({
                        $or: [
                            { name: { $regex: regex } },
                            { email: { $regex: regex } },
                            { phone: { $regex: regex } }
                        ]
                    })
                    return await response.exec()
                }
                const regex = new RegExp(args.searching.key, 'i')
                return await Userdb.find({
                    $or: [
                        { name: { $regex: regex } },
                        { email: { $regex: regex } },
                        { phone: { $regex: regex } }
                    ]
                })
            } catch (error) {
                console.log("getUsers" + error)
                throw new Error(error)
            }
        },
    },
    User: {
        posts: async (parent: systemVariant, _: systemVariant, context: systemVariant) => {
            const posts = Postdb.find({ author: parent._id, status: ACCEPTED })
            const page = context.conditions?.page || 1
            const limit = context.conditions?.limit || process.env.LIMIT_DEFAULT
            const skip = (page - 1) * limit
            posts.skip(skip).limit(limit)
            return await posts.exec()
        },
        friends: async (parent: systemVariant, _: systemVariant) => await Userdb.find({
            _id: { $in: parent.friends }
        })

    },
    Mutation: {
        updateProfile: async (_: systemVariant, args: systemVariant, context: systemVariant) => {
            try {
                const data = args.data
                if (data.avatar) {
                    const user = await Userdb.findById(context.user._id)
                    const status = !(user?.avatar?.filename === OUT_SRC) && await destroyMedia(user?.avatar?.filename)
                    if (status && status.result !== CLOUD_SUCCESS) throw new Error('Error in replace media ' + status.result)
                }
                return await Userdb.findByIdAndUpdate(context.user._id, { $set: data }, { new: true })
            } catch (error) {
                console.log("updateProfile" + error)
                throw new Error(error)
            }
        }
    }
}

export const userTypeDefs = `#graphql
    type Avatar {
        filename: String!
        src: String!
    }

    type User {
        _id: ID
        name: String
        bio: String
        phone: String
        age: String
        email: String
        avatar: Avatar
        status: String
        gender: String
        address: String
        typeLogin: String
        posts: [Post]
        friends: [User]
        role_code: String
    }


    input AvatarObject {
        filename: String!
        src: String!
      }

    input Profile {
        name: String
        bio: String
        phone: String
        age: String
        email: String
        gender: String
        status: String
        address: String
        avatar: AvatarObject
    }

    input UsersFilter {
        key: String
        isFriend: Boolean
    }

    input ProfileFilter {
        _id: ID
        page: Int
        limit: Int
    }

    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    type Query {
        getProfile(searching: ProfileFilter): User!
        getUsers(searching: UsersFilter): [User]
    }
    type Mutation {
        updateProfile(data: Profile) : User
    }
`
