import { systemVariant } from '../types'
import Roomdb from '../modules/rooms'
import Messagedb from '../modules/messages'
import Userdb from '../modules/users'

export const roomResolvers = {
    Query: {
        getAllRooms: async (_: systemVariant, _args: systemVariant, context: systemVariant) => {
            return await Roomdb.find({ members: { $in: [context.user._id] } })
        },
        getRoom: async (_: systemVariant, args: systemVariant) => {
            return await Roomdb.findById(args._id)
        }
    },
    Room: {
        members: async (parent: systemVariant, _: systemVariant) => {
            return await Userdb.find({ _id: { $in: parent.members } })
        },
        messages: async (parent: systemVariant, _: systemVariant) => {
            return await Messagedb.find({ roomID: parent._id })
        }
    },
    Mutation: {
        createRoom: async (_: any, args: systemVariant) => {
            return await Roomdb.create({ members: args.info })
        },
        deleteRoom: async (_: any, args: systemVariant) => {
            const response = await Roomdb.findOneAndDelete({ _id: args._id })
            await Messagedb.deleteMany({ roomID: response?._id })
            return response
        }
    }
};


export const roomTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.

    # This "Comment" type defines the queryable fields for every post in our data source.
    type Room {
      _id: ID
      members: [User]
      messages: [Message]
    }
  
    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    type Query {
        getAllRooms: [Room]
        getRoom(_id: ID!): Room
    }
  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createRoom(info: [ID!]): Room
        deleteRoom(_id: ID!): Room
    }
  `;