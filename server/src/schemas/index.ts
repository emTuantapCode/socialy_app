import { mergeResolvers, mergeTypeDefs } from '@graphql-tools/merge'
import { postTypeDefs, postResolvers } from './post'
import { userTypeDefs, userResolvers } from './user'
import { commentTypeDefs, commentResolvers } from './comment'
import { roomTypeDefs, roomResolvers } from './room'
import { messageTypeDefs, messageResolvers } from './message'
import { notifyTypeDefs, notifyResolvers } from './notification'

export const typeDefs = mergeTypeDefs([postTypeDefs, userTypeDefs, commentTypeDefs, roomTypeDefs, messageTypeDefs, notifyTypeDefs])
export const resolvers = mergeResolvers([postResolvers, userResolvers, commentResolvers, roomResolvers, messageResolvers, notifyResolvers])


