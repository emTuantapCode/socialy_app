"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = __importDefault(require("configs/socket"));
const initialSocketIO = (httpServer) => {
    const io = (0, socket_1.default)(httpServer);
    io.on('connection', (socket) => {
        console.log(`User Connected: ${socket.id}`);
    });
};
exports.default = initialSocketIO;
//# sourceMappingURL=index.js.map