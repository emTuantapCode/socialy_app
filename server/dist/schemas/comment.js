"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.commentTypeDefs = exports.commentResolvers = void 0;
const comments_1 = __importDefault(require("../modules/comments"));
const users_1 = __importDefault(require("../modules/users"));
const posts_1 = __importDefault(require("../modules/posts"));
exports.commentResolvers = {
    Comment: {
        post: async (parent, _) => {
            return await posts_1.default.findById(parent?.post);
        },
        favorite: async (parent, _, context) => {
            return parent?.favorites.includes(context.user._id);
        },
        favoriteNumber: async (parent) => {
            return parent?.favorites.length;
        },
        author: async (parent) => {
            return await users_1.default.findById(parent.author);
        }
    },
    Mutation: {
        createComment: async (_, args, context) => {
            return await comments_1.default.create({ ...args.data, author: context.user._id });
        },
        changeFavoriteComment: async (_, args, context) => {
            try {
                const comment = await comments_1.default.findById(args._id);
                if (!comment)
                    throw new Error('Can not find comment');
                const index = comment?.favorites.indexOf(context.user._id);
                if (index !== -1) {
                    comment?.favorites.splice(index, 1);
                }
                else {
                    comment?.favorites.push(context.user._id);
                }
                await comment.save();
                return comment;
            }
            catch (error) {
                console.log("changeFavoriteComment" + error);
                throw new Error(error);
            }
        },
        deleteComment: async (_, args, context) => {
            return await comments_1.default.findOneAndDelete({ _id: args._id, author: context.user._id });
        }
    }
};
exports.commentTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.
  
    input fieldsComment {
      post: ID!
      content: String!
    }
  
    # This "Comment" type defines the queryable fields for every post in our data source.
    type Comment {
      _id: ID
      author: User
      post: Post
      content: String
      favorite: Boolean
      favoriteNumber: Int
      createdAt: String
    }
  
    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this

  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createComment(data: fieldsComment): Comment
        changeFavoriteComment(_id: ID!): Comment
        deleteComment(_id: ID!): Comment
    }
  `;
//# sourceMappingURL=comment.js.map