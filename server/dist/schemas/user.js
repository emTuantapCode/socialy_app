"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userTypeDefs = exports.userResolvers = void 0;
const users_1 = __importDefault(require("../modules/users"));
const posts_1 = __importDefault(require("../modules/posts"));
const cloud_1 = require("../configs/cloud");
const OUT_SRC = 'googleSRC';
const CLOUD_SUCCESS = 'ok';
const ACCEPTED = 'Pending';
exports.userResolvers = {
    Query: {
        getProfile: async (_, args, context) => {
            try {
                context.conditions = args.searching;
                return await users_1.default.findById({ _id: args.searching._id || context.user._id });
            }
            catch (error) {
                console.log("getProfile" + error);
                throw new Error(error);
            }
        },
        getUsers: async (_, args, context) => {
            try {
                if (args.searching.isFriend) {
                    const { friends } = await users_1.default.findById(context.user._id);
                    const response = users_1.default.find({
                        _id: { $in: friends }
                    });
                    const regex = new RegExp(args.searching.key, 'i');
                    args.searching?.key && response.find({
                        $or: [
                            { name: { $regex: regex } },
                            { email: { $regex: regex } },
                            { phone: { $regex: regex } }
                        ]
                    });
                    return await response.exec();
                }
                const regex = new RegExp(args.searching.key, 'i');
                return await users_1.default.find({
                    $or: [
                        { name: { $regex: regex } },
                        { email: { $regex: regex } },
                        { phone: { $regex: regex } }
                    ]
                });
            }
            catch (error) {
                console.log("getUsers" + error);
                throw new Error(error);
            }
        },
    },
    User: {
        posts: async (parent, _, context) => {
            const posts = posts_1.default.find({ author: parent._id, status: ACCEPTED });
            const page = context.conditions?.page || 1;
            const limit = context.conditions?.limit || process.env.LIMIT_DEFAULT;
            const skip = (page - 1) * limit;
            posts.skip(skip).limit(limit);
            return await posts.exec();
        },
        friends: async (parent, _) => await users_1.default.find({
            _id: { $in: parent.friends }
        })
    },
    Mutation: {
        updateProfile: async (_, args, context) => {
            try {
                const data = args.data;
                if (data.avatar) {
                    const user = await users_1.default.findById(context.user._id);
                    const status = !(user?.avatar?.filename === OUT_SRC) && await (0, cloud_1.destroyMedia)(user?.avatar?.filename);
                    if (status && status.result !== CLOUD_SUCCESS)
                        throw new Error('Error in replace media ' + status.result);
                }
                return await users_1.default.findByIdAndUpdate(context.user._id, { $set: data }, { new: true });
            }
            catch (error) {
                console.log("updateProfile" + error);
                throw new Error(error);
            }
        }
    }
};
exports.userTypeDefs = `#graphql
    type Avatar {
        filename: String!
        src: String!
    }

    type User {
        _id: ID
        name: String
        bio: String
        phone: String
        age: String
        email: String
        avatar: Avatar
        status: String
        gender: String
        address: String
        typeLogin: String
        posts: [Post]
        friends: [User]
        role_code: String
    }


    input AvatarObject {
        filename: String!
        src: String!
      }

    input Profile {
        name: String
        bio: String
        phone: String
        age: String
        email: String
        gender: String
        status: String
        address: String
        avatar: AvatarObject
    }

    input UsersFilter {
        key: String
        isFriend: Boolean
    }

    input ProfileFilter {
        _id: ID
        page: Int
        limit: Int
    }

    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    type Query {
        getProfile(searching: ProfileFilter): User!
        getUsers(searching: UsersFilter): [User]
    }
    type Mutation {
        updateProfile(data: Profile) : User
    }
`;
//# sourceMappingURL=user.js.map