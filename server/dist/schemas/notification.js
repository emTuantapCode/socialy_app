"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.notifyTypeDefs = exports.notifyResolvers = void 0;
const notifications_1 = __importDefault(require("../modules/notifications"));
const users_1 = __importDefault(require("../modules/users"));
exports.notifyResolvers = {
    Query: {
        getNotifications: async (_, _args, context) => {
            return notifications_1.default.find({ destination: context.user._id });
        }
    },
    Notify: {
        source: async (parent, _) => {
            return await users_1.default.findById(parent.source);
        },
        destination: async (parent, _) => {
            return await users_1.default.findById(parent.destination);
        }
    },
    Mutation: {
        createNotify: async (_, args) => {
            return await notifications_1.default.create({ members: args.info });
        },
        deleteNotify: async (_, args) => {
            const response = await notifications_1.default.findOneAndDelete({ _id: args._id });
            await notifications_1.default.deleteMany({ roomID: response?._id });
            return response;
        }
    }
};
exports.notifyTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.

    # This "Comment" type defines the queryable fields for every post in our data source.
    type Notify {
      _id: ID
      type: String
      source: User
      destination: User
      status: String
      uri: String
    }
  
    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    type Query {
        getNotifications: [Notify]
    }
  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createNotify(info: [ID!]): Room
        deleteNotify(_id: ID!): Room
    }
  `;
//# sourceMappingURL=notification.js.map