"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.messageTypeDefs = exports.messageResolvers = void 0;
const rooms_1 = __importDefault(require("../modules/rooms"));
const messages_1 = __importDefault(require("../modules/messages"));
const users_1 = __importDefault(require("../modules/users"));
exports.messageResolvers = {
    Message: {
        sender: async (parent, _) => {
            return await users_1.default.findById(parent.sender);
        },
        roomID: async (parent, _) => {
            return await rooms_1.default.findById(parent.roomID);
        }
    },
    Mutation: {
        createMessage: async (_, args, context) => {
            return await messages_1.default.create({ ...args.info, sender: context.user._id });
        }
    }
};
exports.messageTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.

    input MessageInfo {
        roomID: ID!
        content: String!
    }

    # This "Comment" type defines the queryable fields for every post in our data source.
    type Message {
      _id: ID
      roomID: Room
      sender: User
      content: String
    }
  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createMessage(info: MessageInfo!): Message
    }
  `;
//# sourceMappingURL=message.js.map