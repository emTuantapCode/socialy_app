"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const typeDefs = `#graphql
  # The implementation for this scalar is provided by the
  # 'GraphQLUpload' export from the 'graphql-upload-ts' package
  # in the resolvers file.

  input MediaObject {
    filename: String!
    mediasrc: String!
  }

  type Media {
    filename: String!
    mediasrc: String!
  }

  input Searching {
    _id: ID
    author: String
    status: String
  }

  # This "Posts" type defines the queryable fields for every post in our data source.
  type Posts {
    title: String
    author : String
    content : String
    felling : String
    media: [Media]
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  type Query {
    getPost(searching: Searching!): [Posts]
  }

  # The "Mutation" type is speacial: it lists all of the available features that
  # clients can execute. In this
  type Mutation {
    createPost(title: String! , content: String! , felling: String, media: [MediaObject], author: ID!): Posts
  }
`;
exports.default = typeDefs;
//# sourceMappingURL=typedefs.js.map