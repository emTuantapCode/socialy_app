"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolvers = exports.typeDefs = void 0;
const merge_1 = require("@graphql-tools/merge");
const post_1 = require("./post");
const user_1 = require("./user");
const comment_1 = require("./comment");
const room_1 = require("./room");
const message_1 = require("./message");
const notification_1 = require("./notification");
exports.typeDefs = (0, merge_1.mergeTypeDefs)([post_1.postTypeDefs, user_1.userTypeDefs, comment_1.commentTypeDefs, room_1.roomTypeDefs, message_1.messageTypeDefs, notification_1.notifyTypeDefs]);
exports.resolvers = (0, merge_1.mergeResolvers)([post_1.postResolvers, user_1.userResolvers, comment_1.commentResolvers, room_1.roomResolvers, message_1.messageResolvers, notification_1.notifyResolvers]);
//# sourceMappingURL=index.js.map