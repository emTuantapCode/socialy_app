"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.roomTypeDefs = exports.roomResolvers = void 0;
const rooms_1 = __importDefault(require("../modules/rooms"));
const messages_1 = __importDefault(require("../modules/messages"));
const users_1 = __importDefault(require("../modules/users"));
exports.roomResolvers = {
    Query: {
        getAllRooms: async (_, _args, context) => {
            return await rooms_1.default.find({ members: { $in: [context.user._id] } });
        },
        getRoom: async (_, args) => {
            return await rooms_1.default.findById(args._id);
        }
    },
    Room: {
        members: async (parent, _) => {
            return await users_1.default.find({ _id: { $in: parent.members } });
        },
        messages: async (parent, _) => {
            return await messages_1.default.find({ roomID: parent._id });
        }
    },
    Mutation: {
        createRoom: async (_, args) => {
            return await rooms_1.default.create({ members: args.info });
        },
        deleteRoom: async (_, args) => {
            const response = await rooms_1.default.findOneAndDelete({ _id: args._id });
            await messages_1.default.deleteMany({ roomID: response?._id });
            return response;
        }
    }
};
exports.roomTypeDefs = `#graphql
    # The implementation for this scalar is provided by the
    # in the resolvers file.

    # This "Comment" type defines the queryable fields for every post in our data source.
    type Room {
      _id: ID
      members: [User]
      messages: [Message]
    }
  
    # The "Query" type is special: it lists all of the available queries that
    # clients can execute, along with the return type for each. In this
    type Query {
        getAllRooms: [Room]
        getRoom(_id: ID!): Room
    }
  
    # The "Mutation" type is speacial: it lists all of the available features that
    # clients can execute. In this
    type Mutation {
        createRoom(info: [ID!]): Room
        deleteRoom(_id: ID!): Room
    }
  `;
//# sourceMappingURL=room.js.map