"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const posts_1 = __importDefault(require("../modules/posts"));
const resolvers = {
    Query: {
        getPost: async (_, args) => {
            console.log(args);
            return await posts_1.default.find();
        },
    },
    Mutation: {
        createPost: async (_, args) => {
            return await posts_1.default.create(args);
        }
    }
};
exports.default = resolvers;
//# sourceMappingURL=resolvers.js.map