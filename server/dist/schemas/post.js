"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.postTypeDefs = exports.postResolvers = void 0;
const posts_1 = __importDefault(require("../modules/posts"));
const users_1 = __importDefault(require("../modules/users"));
const comments_1 = __importDefault(require("../modules/comments"));
const cloud_1 = require("../configs/cloud");
const ADMIN = 'f0';
exports.postResolvers = {
    Query: {
        getPost: async (_, args, context) => {
            try {
                const conditions = args.searching;
                const { friends } = await users_1.default.findById(context.user._id);
                const posts = posts_1.default.find({
                    author: {
                        $in: friends
                    }
                });
                posts.sort({ 'createdAt': -1 });
                const page = conditions?.page || 1;
                const limit = conditions?.limit || process.env.LIMIT_DEFAULT;
                const skip = (page - 1) * limit;
                posts.skip(skip).limit(limit);
                return await posts.exec();
            }
            catch (error) {
                console.log('getPost ' + error);
                throw new Error(error);
            }
        },
        getPostAdmin: async (_, args, context) => {
            try {
                if (context.user.role_code !== ADMIN)
                    throw new Error('require admin role');
                const conditions = args.searching;
                const queries = { status: conditions.status };
                if (conditions.startDate && conditions.endDate) {
                    queries.createdAt = { '$gt': conditions.startDate, '$lt': conditions.endDate };
                }
                const response = posts_1.default.find(queries);
                const page = conditions?.page || 1;
                const limit = conditions?.limit || process.env.LIMIT_DEFAULT;
                const skip = (page - 1) * limit;
                response.skip(skip).limit(limit);
                return await response.exec();
            }
            catch (error) {
                console.log('getPostAdmin ' + error);
                throw new Error(error);
            }
        },
    },
    Post: {
        author: async (parent, _) => {
            return await users_1.default.findById(parent?.author);
        },
        favoriters: async (parent, _) => {
            const response = await users_1.default.find({ _id: { $in: parent?.favorites } }).select('_id name avatar');
            return response;
        },
        favorited: async (parent, _, context) => {
            return parent?.favorites.includes(context.user._id);
        },
        favoriteNumber: async (parent) => {
            return parent?.favorites.length;
        },
        comment: async (parent, _) => {
            return await comments_1.default.find({ post: parent._id });
        },
        commentNumber: async (parent) => {
            return await comments_1.default.countDocuments({ post: parent._id });
        },
    },
    Mutation: {
        createPost: async (_, args, context) => {
            args.author = context.user._id;
            return await posts_1.default.create({ ...args });
        },
        updatePost: async (_, args, context) => {
            try {
                const { _id, prevMedia, ...res } = args;
                prevMedia && prevMedia.forEach(async (media) => {
                    const response = await (0, cloud_1.destroyMedia)(media?.filename);
                });
                return await posts_1.default.findOneAndUpdate({ _id, author: context.user._id }, {
                    $set: res
                }, { new: true });
            }
            catch (error) {
                console.log('updatePost ' + error);
                throw new Error(error);
            }
        },
        changeFavoritePost: async (_, args, context) => {
            try {
                const post = await posts_1.default.findById(args._id);
                if (!post)
                    throw new Error('Can not find comment');
                const index = post?.favorites.indexOf(context.user._id);
                if (index !== -1) {
                    post?.favorites.splice(index, 1);
                }
                else {
                    post?.favorites.push(context.user._id);
                }
                await post.save();
                console.log(post);
                return post;
            }
            catch (error) {
                console.log("changeFavoritePost" + error);
                throw new Error(error);
            }
        },
        deletePost: async (_, args, context) => {
            try {
                const post = await posts_1.default.findOneAndRemove({ _id: args._id, author: context.user._id });
                post?.media.forEach(async (media) => {
                    const cloud = await (0, cloud_1.destroyMedia)(media?.filename);
                });
                return post ? true : false;
            }
            catch (error) {
                console.log('deletePost ' + error);
                throw new Error(error);
            }
        },
    }
};
exports.postTypeDefs = `#graphql
  # The implementation for this scalar is provided by the
  # 'GraphQLUpload' export from the 'graphql-upload-ts' package
  # in the resolvers file.

  input MediaObject {
    filename: String!
    mediasrc: String!
  }

  type Media {
    filename: String!
    mediasrc: String!
  }

  input filerPost {
    page: String
    limit: String
  }

  input filerPostAdmin {
    status: String !
    startDate: String
    endDate: String
    page: Int
    limit: Int
  }

  # This "Posts" type defines the queryable fields for every post in our data source.
  type Post {
    _id: ID
    color: String
    author : User
    content : String
    felling : String
    media: [Media]
    favoriters: [User]
    favorited: Boolean
    favoriteNumber: Int
    comment: [Comment]
    commentNumber: Int
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each. In this
  type Query {
    getPost(searching: filerPost!): [Post]
    getPostAdmin(searching: filerPostAdmin!): [Post]
  }

  # The "Mutation" type is speacial: it lists all of the available features that
  # clients can execute. In this
  type Mutation {
    createPost(color: String! , content: String! , felling: String, media: [MediaObject]): Post
    updatePost(_id:ID!, color: String , content: String , felling: String, media: [MediaObject]): Post
    changeFavoritePost(_id: ID!): Post
    deletePost(_id:ID!): Post
  }
`;
//# sourceMappingURL=post.js.map