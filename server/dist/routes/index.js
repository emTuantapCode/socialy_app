"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express4_1 = require("@apollo/server/express4");
const dist_1 = require("graphql-upload-ts/dist");
const passport_1 = __importDefault(require("passport"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const initialAPIs = (app, server) => {
    app.get('/api/google/service', passport_1.default.authenticate('google', { scope: ['profile', 'email'] }));
    app.get('/api/google/callback', (req, res, next) => {
        passport_1.default.authenticate('google', (err, user) => {
            if (err)
                return res.status(503).json('Error in auth 2.0');
            req.user = {
                isNew: user[0].oganizationid ? 0 : 1,
                id: user[0].id
            };
            next();
        })(req, res, next);
    }, (req, res) => {
        const token = jsonwebtoken_1.default.sign({ id: req.user?.id, isNew: req.user?.isNew }, process.env.JWT_SECRET, { expiresIn: '1d' });
        res.redirect(`${process.env.URL_CLINET}/login-success?is-new=${req.user?.isNew}&id=${req.user?.id}&token=${token}`);
    });
    app.post('/api/google/success', async () => {
    });
    app.use('/api/graphql', (0, dist_1.graphqlUploadExpress)(), (0, express4_1.expressMiddleware)(server, {
        context: async ({ req }) => ({ token: req.headers.token }),
    }));
    app.use((res) => res.status(404).json('Can not find page'));
};
exports.default = initialAPIs;
//# sourceMappingURL=index.js.map