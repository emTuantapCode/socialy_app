"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("@apollo/server");
const drainHttpServer_1 = require("@apollo/server/plugin/drainHttpServer");
const express_1 = __importDefault(require("express"));
const http_1 = __importDefault(require("http"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const schemas_1 = require("./schemas");
const mongo_1 = __importDefault(require("./configs/mongo"));
const dotenv_1 = __importDefault(require("dotenv"));
const apis_1 = __importDefault(require("./apis"));
const socket_io_1 = __importDefault(require("./socket_io"));
const passport_1 = __importDefault(require("./configs/passport"));
dotenv_1.default.config();
const app = (0, express_1.default)();
const httpServer = http_1.default.createServer(app);
const server = new server_1.ApolloServer({
    typeDefs: schemas_1.typeDefs,
    resolvers: schemas_1.resolvers,
    plugins: [(0, drainHttpServer_1.ApolloServerPluginDrainHttpServer)({ httpServer })]
});
(async () => {
    (0, passport_1.default)();
    (0, socket_io_1.default)(httpServer);
    await server.start();
    app.use((0, cors_1.default)({
        origin: process.env.CLIENT_URI
    }), body_parser_1.default.json());
    (0, apis_1.default)(app, server);
    await new Promise((resolve) => httpServer.listen({ port: process.env.PORT }, resolve));
    await (0, mongo_1.default)();
    console.log(`🚀 Server ready at http://localhost:${process.env.PORT}`);
    console.log(`🚀 cors origin at ${process.env.CLIENT_URI}`);
})();
//# sourceMappingURL=server.js.map