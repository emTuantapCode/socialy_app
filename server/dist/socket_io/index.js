"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const socket_1 = __importDefault(require("../configs/socket"));
var onlineUsers = [];
const addUser = (user) => {
    onlineUsers.push(user);
};
const removeUser = (leaveUser) => {
    onlineUsers = onlineUsers.filter(user => user._id !== leaveUser._id);
};
const filterOnline = (people) => {
    return onlineUsers.filter(user => people.includes(user._id));
};
const initialSocketIO = (httpServer) => {
    const io = (0, socket_1.default)(httpServer);
    io.on('connection', (socket) => {
        socket.on('addUser', ({ user, friendList }) => {
            user.socketID = socket.id;
            addUser(user);
            const onlineFriends = filterOnline(friendList);
            onlineFriends.length !== 0 && onlineFriends.forEach(friend => {
                io.to(friend?.socketID).emit('getOnlineUser', {
                    data: onlineFriends
                });
            });
        });
        socket.on('approvedPost', (info) => {
            io.to(info?.user?.socketID).emit('getPostStatus', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was approved by admin`
            });
            const onlineFriends = filterOnline(info?.friendList);
            onlineFriends.length !== 0 && onlineFriends.forEach(friend => {
                io.to(friend?.socketID).emit('getPost', {
                    mess: `${info.user.name} has a new post ${info.content ? ': ' + info.content : ''}`
                });
            });
        });
        socket.on('deniedPost', (info) => {
            io.to(info?.user?.socketID).emit('getPostStatus', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was denied by admin`
            });
        });
        socket.on('addComment', (info) => {
            io.to(info?.destination?.socketID).emit('getComment', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was denied by admin`
            });
        });
        socket.on('addComment', (info) => {
            io.to(info?.destination?.socketID).emit('getComment', {
                mess: `Your post ${info.content ? ': ' + info.content : ''} was denied by admin`
            });
        });
        socket.on('addMessage', (info) => {
            const onlineMembers = filterOnline(info?.membersList);
            onlineMembers.length !== 0 && onlineMembers.forEach(member => {
                io.to(member?.socketID).emit('getMessage', {
                    room: info.roomID,
                    mess: `${info.source?.name} sent you a message`,
                    content: info.content
                });
            });
        });
        socket.on('disconnection', ({ user, friendList }) => {
            removeUser(user);
            const onlineFriends = onlineUsers.filter(user => friendList.includes(user._id));
            onlineFriends.length !== 0 && onlineFriends.forEach(friend => {
                io.to(friend?.socketID).emit('getOnlineUser', {
                    data: onlineFriends
                });
            });
        });
    });
};
exports.default = initialSocketIO;
//# sourceMappingURL=index.js.map