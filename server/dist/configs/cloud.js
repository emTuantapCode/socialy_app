"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.destroyMedia = exports.uploadCloud = void 0;
const cloudinary_1 = __importDefault(require("cloudinary"));
const multer_storage_cloudinary_1 = require("multer-storage-cloudinary");
const multer_1 = __importDefault(require("multer"));
cloudinary_1.default.v2.config({
    cloud_name: process.env.CLOUD_NAME || 'dciozzifg',
    api_key: process.env.CLOUD_KEY || '956686345645342',
    api_secret: process.env.CLOUD_SECRET || 'AxIvq1VWdk0jLmrZgQ7qz2tGYrs',
});
const storage = new multer_storage_cloudinary_1.CloudinaryStorage({
    cloudinary: cloudinary_1.default.v2,
    allowedFormats: ['jpg', 'png'],
    params: {
        folder: process.env.CLOUD_FOLDER || 'graphql_cloud',
    }
});
exports.uploadCloud = (0, multer_1.default)({
    storage,
});
const destroyMedia = async (fileName) => {
    cloudinary_1.default.v2.config({
        cloud_name: process.env.CLOUD_NAME,
        api_key: process.env.CLOUD_KEY,
        api_secret: process.env.CLOUD_SECRET,
    });
    try {
        return await cloudinary_1.default.v2.uploader.destroy(fileName);
    }
    catch (error) {
        console.log(`Cloud error: ${error}`);
    }
};
exports.destroyMedia = destroyMedia;
//# sourceMappingURL=cloud.js.map