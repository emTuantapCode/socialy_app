"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const users_1 = __importDefault(require("../modules/users"));
const passport_google_oauth20_1 = require("passport-google-oauth20");
const startGoogleStrategy = () => {
    passport_1.default.use(new passport_google_oauth20_1.Strategy({
        clientID: process.env.GOOGLE_ID,
        clientSecret: process.env.GOOGLE_SECRET,
        callbackURL: "/api/google/callback",
        scope: ['profile', 'email'],
    }, async (accessToken, refreshToken, profile, cb) => {
        try {
            if (profile?.id) {
                const user = await users_1.default.findOne({ googleID: profile.id });
                if (user) {
                    const response = {
                        isNew: false,
                        user: user
                    };
                    return cb(null, response);
                }
                const newUser = await users_1.default.create({
                    googleID: profile.id,
                    email: profile._json.email,
                    name: profile._json.name,
                    typeLogin: profile.provider,
                    role_code: (profile._json.email != process.env.EMAIL_ADMIN)
                        ? process.env.ROLE_DEFAULT : process.env.ROLE_ADMIN,
                    avatar: {
                        filename: 'googleSRC',
                        src: profile.photos[0]?.value
                    },
                });
                const response = {
                    isNew: true,
                    user: newUser
                };
                return cb(null, response);
            }
            return cb(null, profile);
        }
        catch (error) {
            console.log(error);
            return cb(error, null);
        }
    }));
};
exports.default = startGoogleStrategy;
//# sourceMappingURL=passport.js.map