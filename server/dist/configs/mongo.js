"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const dbConnect = async () => {
    try {
        await mongoose_1.default.connect(process.env.MONGODB_URI || 'mongodb://0.0.0.0:27017/');
        console.log('🎃 DB connected successfully!');
    }
    catch (error) {
        console.log(`XD DB connection failed!`);
        throw new Error(error);
    }
};
exports.default = dbConnect;
//# sourceMappingURL=mongo.js.map