"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_io_1 = require("socket.io");
const configSocketIO = (httpServer) => {
    const io = new socket_io_1.Server(httpServer, {
        cors: {
            origin: "*"
        },
    });
    return io;
};
exports.default = configSocketIO;
//# sourceMappingURL=socket.js.map