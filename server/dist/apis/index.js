"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express4_1 = require("@apollo/server/express4");
const dist_1 = require("graphql-upload-ts/dist");
const passport_1 = __importDefault(require("passport"));
const jwt_1 = require("../middlewares/jwt");
const authorization_1 = require("../middlewares/authorization");
const cloud_1 = require("../configs/cloud");
const initialAPIs = (app, server) => {
    app.get('/api/google/service', passport_1.default.authenticate('google', { scope: ['profile', 'email'] }));
    app.get('/api/google/callback', (req, res, next) => {
        passport_1.default.authenticate('google', (err, response) => {
            if (err)
                return res.status(502).json('Error :' + err);
            req.user = response.user;
            req.isNew = response.isNew;
            next();
        })(req, res, next);
    }, (req, res) => {
        const token = (0, jwt_1.generateCommonToken)(req.user?._id, req.user?.role_code, '1d');
        res.redirect(`${process.env.CLIENT_URI}/login-success?is-new=${req.isNew}&token=${token}`);
    });
    app.post('/api/cloudinary/single-upload', authorization_1.requireToken, cloud_1.uploadCloud.single('myfile'), (req, res) => {
        try {
            const response = {
                filename: req.file?.filename,
                src: req.file?.path
            };
            return res.status(200).json(response);
        }
        catch (error) {
            throw new Error(error);
        }
    });
    app.post('/api/cloudinary/multil-upload', authorization_1.requireToken, cloud_1.uploadCloud.any(), (req, res) => {
        try {
            const response = [];
            const files = req.files;
            files.map((file) => {
                const element = {
                    filename: file.filename,
                    mediasrc: file.path
                };
                response.push(element);
            });
            return res.status(200).json(response);
        }
        catch (error) {
            throw new Error(error);
        }
    });
    app.use('/api/graphql', authorization_1.requireToken, (0, dist_1.graphqlUploadExpress)(), (0, express4_1.expressMiddleware)(server, {
        context: async ({ req }) => ({ user: req.headers.identify }),
    }));
    app.use((res) => res.status(404).json('Can not find page'));
};
exports.default = initialAPIs;
//# sourceMappingURL=index.js.map