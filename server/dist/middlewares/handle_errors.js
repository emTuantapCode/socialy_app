"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.notFound = void 0;
const notFound = (req, res, next) => {
    const error = new Error(`Route ${req.originalUrl} not found!`);
    return res.status(404).json(`Route ${req.originalUrl} not found!`);
};
exports.notFound = notFound;
//# sourceMappingURL=handle_errors.js.map