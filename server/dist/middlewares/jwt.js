"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateCommonToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const generateCommonToken = (_id, role_code, expiresIn) => {
    return jsonwebtoken_1.default.sign({ _id, role_code }, process.env.JWT_SECRET, { expiresIn: expiresIn });
};
exports.generateCommonToken = generateCommonToken;
//# sourceMappingURL=jwt.js.map