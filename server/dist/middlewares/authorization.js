"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.requireToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const requireToken = (req, res, next) => {
    let token = req.headers.authorization;
    if (!token) {
        return res.status(401).json('Require authorization token');
    }
    else {
        const err = jsonwebtoken_1.default.verify(token, process.env.JWT_SECRET, (err, decode) => {
            req.headers.identify = decode;
            return err;
        });
        if (err)
            return res.status(401).json('Unauthorized token');
        next();
    }
};
exports.requireToken = requireToken;
//# sourceMappingURL=authorization.js.map