# SocialyApp Đinh Tuấn Anh - Alan ![applogo](https://media.licdn.com/dms/image/C5612AQGmtVoLmWt_0w/article-cover_image-shrink_600_2000/0/1520250009922?e=2147483647&v=beta&t=YjXMKKCexVvXxSzkzesYc3Nr-QQ16IRwPRUYpqBWyp0)

Trang web như một phần mêm mạng xã hội sơ khai gồm các tính năng cơ bản như:

🎃Đăng ký đăng nhập với google

🎃Chia sẻ các bài post hay các videos khoảnh khắc

🎃Tính năng yêu thích, bình luận

🎃Tính năng kết bạn để theo dõi

🎃Tính năng nhắn tin

🎃Thông báo thời gian thực

## Cài đặt

Ứng dụng sử dụng gói quản lý thư viện [npm](https://www.npmjs.com/package/react-html-parser) 

```bash
git clone <socialyApp path>
cd <server || client-graphql-app>
#Sử dụng câu lệnh tại <server || client-graphql-app> để cài đặt các thư viện cần thiết
npm i
#Đối với server để khởi chạy
npm run dev
#Đối với client sử dụng tsc để biên dịch typescript thành commonscript
#Sau khi xuất hiện file dist bạn có thể khởi chạy client bằng câu lệnh dưới
npm run dev
```

## Cấu trúc cơ sở dữ liệu ( cơ sở dữ liệu phân tán )
Sử dụng cơ sở dữ liệu mongoDB kết hợp với cơ sở dữ liệu đám mây [cloudinary](https://cloudinary.com/) đê tối ưu hiệu năng truy vấn và cấu trúc cơ sở dữ liệu

[Tài liệu thiết kế database tại đây !!!](https://docs.google.com/document/d/1Sy4W_YK__6Il7DHV5WVDz2-gDT-6_YQceOd1t1o9lHc/edit?usp=sharing)
```typescript
//Config cloudinary service
import cloudinary from 'cloudinary'

cloudinary.v2.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_KEY,
    api_secret: process.env.CLOUD_SECRET,
})
```
## Ngôn ngữ hỗ trợ truy vấn cơ sở dữ liệu

Tôi sử dụng [graphQL](https://graphql.org/) - Một ngôn ngữ truy vấn phát hành bởi facebook nhằm tối ưu truy vấn với lượng dữ liệu lớn
```typescript
//Cấu trúc xây dựng truy vấn với graphql
var { graphql, buildSchema } = require("graphql")

var schema = buildSchema(`
  type Query {
    hello: String
  }
`)

var rootValue = { hello: () => "Hello world!" }

var source = "{ hello }"

graphql({ schema, source, rootValue }).then(response => {
  console.log(response)
})
```
## Tài liệu liên quan
[Tài liệu mô tả database](https://docs.google.com/document/d/1Sy4W_YK__6Il7DHV5WVDz2-gDT-6_YQceOd1t1o9lHc/edit?usp=sharing)

[Duy trì kết nối thời gian thực](https://socket.io/)

[Nền tảng để thực hiện ngôn ngữ truy vấn](https://www.apollographql.com/docs/)

[Công cụ khởi tạo dự án](https://vitejs.dev/)

## License

[Quyền sở hữu bởi Alan Dinh - Đinh Tuấn Anh](https://gitlab.com/emTuantapCode)